# A Domain-Specific Language for Coordinating Collaboration

This is the repository for Christoph Laaber's master thesis titled 'A Domain-Specific Language for Coordinating Collaboration'.

The actual thesis is the file `1127107_mt.pdf`.

## Compiling the Thesis
We used TexShop on OSX for simplicity. The thesis uses BibLATEX.

1. TeXShop - LaTeX
2. TeXShop - BibTeX
3. Terminal - `makeglossaries 1127107_mt`
4. TeXShop - LaTeX
5. TeXShop - LaTeX

## Repositories referenced in Thesis
* [DSL Implementation](https://bitbucket.org/chrstphlbr/mt_dsl)
* [hADL Synchronous Library](https://bitbucket.org/chrstphlbr/mt_dsl-lib)
* [Evaluation Process](https://bitbucket.org/chrstphlbr/mt_evaluation)
* [Evaluation Model](https://bitbucket.org/chrstphlbr/mt_evaluation_hadl)
* [Evaluation DSL Script](https://bitbucket.org/chrstphlbr/mt_evaluation_dsl)
* [Agilefant Surrogates, Sensors and REST-Client](https://bitbucket.org/chrstphlbr/mt_agilefant4hadl)
* [Bitbucket and HipChat Surrogates, Sensors and REST-Client](https://bitbucket.org/chrstphlbr/mt_atlassian4hadl)