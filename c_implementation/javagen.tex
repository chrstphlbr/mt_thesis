The next step after specifying the grammar is to translate \gls{dsl} scripts into Java source code. First the following sections define how the desired output should look like and then they give a brief introduction how this is achieved with Xtext. From the chapter on language specification \seeref{d-lang} we know that the \gls{dsl} provides some basic set up statements first and then continues with Task specifications. There are two obvious strategies how translations can be done.

The next subsections contain references to type names of the Java SE \gls{api} \cite{oracle_java_????}.

\subsubsection{Possible Compilation Strategies}

The first strategy is to translate every Task into an own Java class, where the Task name is equal to the class name. Input parameters could translate to properties with setters and output parameters are properties with getters, or input parameters are constructor parameters and the task body translates to the only method of \code{java.util.concurrent.Callable<V>} where \code{V} are the output parameters. Furthermore the code generator needs to compile the set up functionality to either a super class of all Tasks or into a separate class that is accessed from the Tasks through composition. The elegance  of this strategy is that the different parts (Tasks and set up) is spread over multiple classes and with every Task being an instance of \code{Callable<V>} concurrency is easily applicable (through \code{java.util.concurrent.ExecutorService}s). The downsides are that the Java code generation process is more complex and the developer that calls the Tasks must instantiate an object per Task.

The other strategy is to compile everything into a single class. Tasks are translated into methods of that class and input parameters are translated to method parameters. Per design the \gls{dsl} supports multiple output parameters, but Java only supports a single output parameter. The compiler tackles that problem by always returning an instance of \code{java.util.Map<String, Object>}. This strategy does not separate concerns as elegant as the first one, but makes translation easier. Furthermore clients that invoke Tasks only have to instantiate a single object.

The \gls{dsl} implementation takes the second strategy and compiles all Tasks into a single Java class. The name of the class is defined in the set up section with the keyword \code{className}.

\subsubsection{Xtext Compilation Infrastructure}
Xtext provides an automatically generated stub which is the entry point to compiling your own language into Java source code. This class is located in the main sub-project in the package \code{net.laaber.mt.jvmmodel}. It is named \code{DSLJvmModelInferrer} and extends \code{AbstractModelInferrer}. The entry method which is called when translation starts has the signature as shown in Listing \ref{code-jcg-entry-method}. The keyword \code{dispatch} indicates that this method supports multiple dispatch. \code{Domainmodel} is the name of the entry parser rule defined in the grammar file which is provided by Xtext as a class with all variables defined in the grammar accessible. The parameter \code{acceptor} is used for generating Java code by calling the method \code{accept} on it and providing the desired translations as parameter.

\begin{lstlisting}[language=Xtend, caption=Entry Method for Java Code Generation, label=code-jcg-entry-method]
def dispatch void infer(Domainmodel element, IJvmDeclaredTypeAcceptor acceptor, boolean isPreIndexingPhase)
\end{lstlisting}

\subsubsection{Compilation Steps}
The first step of the compilation is to load the \gls{hadl} model that was provided in the \gls{dsl} script and initialise the \code{ModelTypesUtil} from the \gls{hadl} runtime framework with it. The \code{ModelTypesUtil} exports convenience functions to work with the \gls{hadl} model such as retrieving types from \glspl{id}. After that the variable manager is initialised \seeref{i-val}. As last step the actual translation is performed. Translation consists of three main steps: (i) class creation, (ii) set up instructions, and (iii) Task creation.

\paragraph{Class Creation and Set Up Steps} are the first steps during code translation. Listing \ref{code-jcg-class} shows the usage of the parameter \code{acceptor}, the generation the class with the provided set up statement (\code{className}), retrieving variables from the parameter \code{element} and invoking the \code{addSetUp} method. \code{element} and \code{acceptor} are the method parameters from Listing \ref{code-jcg-entry-method}. Line 2 shows the method invocation that returns an instance of a subtype of \code{org.eclipse.xtext.common.types.JvmDeclaredType}. The parameter is the class name that is provided in the set up section of the \gls{dsl} script after the \code{className} statement. Line 3 calls a custom method that adds the \code{setUp} method to the created class, which set ups the \gls{guice} injector needed for the \gls{hadl} runtime framework. It returns a data object that includes all necessary objects (e.g. LinkageConnector, HADLruntimeMonitor) of type \code{net.laaber.mt.dsl.lib.hadl.ProcessScope} which is defined in the \gls{hadl}-lib-project. Line 5 shows a placeholder comment where the actual generation of Tasks is placed.

\begin{lstlisting}[language=Xtend, caption=Java Class Generation and Set Up, label=code-jcg-class]
acceptor.accept(
	element.toClass(element.className) [
		addSetUp(element, members)
		
		// continue with Task generation		
	]
)				
\end{lstlisting}

\paragraph{Task Creation}
The biggest part of the Java code generation is the creation of Tasks. The second scenario describes that every Task is compiled to a method of the previously created class. A code example with detailed description of how Tasks are translated is out of scope of this thesis. The steps of the Task translation are: (i) for every Task a new method with the provided name is created; (ii) Task methods have a \code{throws} clause for \code{java.lang.Throwable} types; (iii) a parameter of type \code{ProcessScope} is added to the signature; (iv) all defined input parameters (\code{in} and \code{javaIn}) are added to the signature; (v) the body of the Task method is specified.

In (iii) the \gls{dsl} implementation adds an input parameter of type \code{ProcessScope}. This parameter is necessary that the \gls{hadl} runtime state with all referenced Java objects can be passed between different Tasks. Otherwise objects might be removed by the Java garbage collector even though they are conceptually still needed.

The Tasks' method body is translated in the following steps into Java code: (i) check if \code{ProcessScope} input is provided. If not call the \code{setUp} method which returns a freshly initialised \code{ProcessScope} variable; (ii) \gls{hadl} input parameter type checks. As the Java type system does not support \gls{hadl} types, the generated code needs to check at runtime if the provided \gls{hadl} input parameters match the specified \gls{hadl} types of the \gls{dsl} script; (iii) create the output map and add the \code{ProcessScope} variable and the failure list to it; and finally (iv) translate the statements \seeref{d-lang} into their Java equivalents.

Algorithm \ref{alg-trans} shows the above described \gls{dsl} to Java translation as pseudo code.

\begin{algorithm}
	\SetKwProg{AddBody}{addBody}{}{end}
	\SetKwProg{Create}{create}{}{end}
	\SetKw{Init}{init}
	\SetKw{Add}{add}
	\SetKw{AddThrows}{addThrows}
	\SetKw{AddParam}{addParam}
	\SetKw{Check}{check}
	\SetKw{Generate}{generate Java code}
	\SetKwFunction{SetUp}{setUp}

	\If{hADL model not loaded}{
		load hADL model
	}
	
	\Init{variable manager}
	
	\Create{Java class}{
		\Add{setUp func}
	
		\ForEach{t of Tasks}{
			\Create{method for t}{
				\AddThrows{Throwable}
				
				\AddParam{ps : ProcessScope}
				
				\Add{other parameters}
				
				\AddBody{}{
					\If{ps == null} {
						ps = \SetUp{}
					}
					
					\Check{hADL parameters for types}
					
					\Add{ps and failure list to output map}
					
					\ForEach{s of Statements}{					
						\Generate
					}
				}
			}
		}
	}
	
	\caption{\gls{dsl} to Java Translation}
	\label{alg-trans}
\end{algorithm}
