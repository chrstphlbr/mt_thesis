After deciding the domain of the \gls{dsl} and designing the concepts of it, the selection of a suitable \gls{dsl} development tool is crucial. Besides supporting the features defined in Chapter \ref{c-design}, the ease of development, relatively flat learning curve and editor/\gls{ide} support are important characteristics as basis for making a choice. There are two ways of implementing a \gls{dsl} \cite{fowler_domain-specific_2010}. The internal and the external approach. The internal \gls{dsl} is a particular \gls{api} as part of a host language, whereas an external \gls{dsl} is independently parsed from the host language. As internal host language Scala \cite{ecole_polytechnique_federale_de_lausanne_epfl_scala_2016} and Haskell \cite{haskell.org_haskell_????} were considered because those languages have a rich type system. Tools for external \gls{dsl} development that were considered are Xtext \cite{eclipse_foundation_inc._xtext_????} and Epsilon \cite{eclipse_foundation_inc._epsilon_????}. We decided to use Xtext as the \gls{dsl} development framework because the documentation seemed good and the assistant advisor of this thesis had previous experience working with it.

Table \ref{tab-sw-versions} shows the versions of the main languages and frameworks that are used for development.

\begin{table}
	\centering
	\begin{tabular}{ll}
		\toprule
		Software			& Version \\
		\midrule
		Eclipse \gls{ide}	& 4.5.1 \\
		Xtext \gls{sdk}		& 2.8.4 \\
		Xtend			& 2.8.4 \\
		\bottomrule
	\end{tabular}
  	\caption{Software Versions used for Development}
  	\label{tab-sw-versions}
\end{table}

\paragraph{Xtend} \cite{eclipse_foundation_inc._xtend_????} is the programming language that is primarily used for developing with Xtext. The \citeauthor{eclipse_foundation_inc._xtend_????} is responsible for developing the language. They advertise Xtend as a modernised Java where features such as extension methods, multiple dispatch (multi methods), everything is an expression and advanced type inference are supported. A language primer of Xtend is out of scope of this thesis. For developers familiar with Java it should be straight forward to learn the basics of Xtend. The Xtend website \cite{eclipse_foundation_inc._xtend_????} and the Xtext website \cite{eclipse_foundation_inc._xtext_????} provide plenty information to learn Xtend. Code examples which use features that are not common to Java developers are explained as they are presented. Xtend compiles into Java source code and therefore supports seamless integration of Java libraries with Xtend programs. This integration feature is of high interest to this work as the \gls{hadl} framework is written in Java itself and tight integration of it and the \gls{dsl} is essential. 

\paragraph{Xtext} is an Eclipse-based framework that provides all features required for developing programming languages and \glspl{dsl}. The Xtext framework shows its power through many examples. One use case is Xtend, which is implemented using Xtext. The Xtext infrastructure includes a powerful grammar language, parser, linker, type checker, compiler, and \gls{ide} features for Eclipse. The following sections describe the syntax specification \seeref{i-grammar}, the translation of \gls{dsl} code into Java code \seeref{i-javagen}, constraint checks through validation \seeref{i-val} and scopes \seeref{i-scopes}, and syntax proposals and auto-completions \seeref{i-auto}.

\subsection{Project Structure}
\label{i-struct}
This section describes how the \gls{dsl}-project is structured. The \gls{dsl}-project consists of multiple sub-projects. All sub-projects but the \textit{net.laaber.mt.DSL.lib} sub-project are defined by Xtext. The sub-projects are listed below.

\begin{itemize}
	\item \textit{net.laaber.mt.DSL} is the main sub-project. It includes the grammar file, \gls{di} set up, the workflow file which defines the set up for the whole generation process and classes for java generation, scoping and validation. Additionally the sub-project includes classes for handling variables and types. Those additional classes are called from the validator, scope provider and proposal provider.

	\item \textit{net.laaber.mt.DSL.lib} is the only sub-project that is not required by Xtext. It is the central point for dealing with external dependencies for all the other projects. Xtext is built as an Eclipse plug-in, which uses \gls{osgi}. \gls{osgi} handles dependencies between bundles through manifest files (META-INF/MANIFEST.MF). \gls{maven} and \gls{osgi} do not mix well, therefore it is not possible letting \gls{maven} handle the dependencies. All dependencies required by the \gls{dsl}-project are in the \textit{libs} folder.

	\item \textit{net.laaber.mt.DSL.sdk} is an automatically generated sub-project where no modifications were made. Hence it is of no further interest to us. 

	\item \textit{net.laaber.mt.DSL.tests} is the sub-project where unit tests for the parser, validators and scopes are put.

	\item \textit{net.laaber.mt.DSL.ui} deals with all \gls{ui} specific functionality. Those include proposal providers, labeling, outline and quick fix. This work only customises the proposal provider. The default implementations of the other features are sufficient for this thesis' purpose.
\end{itemize}



\subsection{Grammar Specification}
\label{i-grammar}
The syntax as described in Section \ref{d-lang} is defined in Xtext through their special grammar file. The file is located in the main sub-project in the package \code{net.laaber.mt} and named \textit{DSL.xtext}. A typical parser can be divided into four stages: (i) lexing, (ii) parsing, (iii) linking and (iv) validating. The first two are defined in the grammar file, the third is declared in the grammar file but needs additional semantic handling \seeref{i-scopes} and the fourth is solely defined through validators \seeref{i-val}.

Not all features of the grammar are used in the \gls{dsl}. The implementation uses existing terminal rules (lexer), defines custom parser rules, calls custom parser rules from other parser rules and defines cross-references which are later used by the linker \seeref{i-scopes}.

\paragraph{Terminal Rules} that the grammar specification uses are \code{ID} and \code{STRING}. \code{ID} specifies an identifier that starts with either a lowercase letter, an uppercase letter or an underscore, followed by a possibly empty set of characters that are either the same as the first character or a number from 0 to 9. \code{STRING} represents a String enclosed by double quotes (\code{"}). The \gls{dsl}'s grammar uses \code{ID}s for Task names, Variable names and ResourceDescriptor factory method names. \code{STRING}s are used as \gls{hadl} file path, Output variable names and ResourceDescriptor factory method parameters.

\pagebreak %formatting reasons

\paragraph{Custom Parser Rules} are the main building blocks in the \gls{dsl}'s grammar file. The first rule specified serves as the entry rule for the parser. Listing \ref{code-grammar-setup} shows the definition of the entry rule, which specifies the set up statements from Section \ref{d-lang-gen}. \code{Domainmodel} is the name of the custom parser rule. Between the colon (\code{:}) and the sem-colon (\code{;}) the body of the rule is defined. Between single quotes (\code{'}) keywords are defined. A \gls{hadl} model file path is specified (line 2) by starting with the keyword \code{hADL} followed by a String (as specified by the terminal rule \code{STRING}). The String is assigned to the variable \code{file} through which it is accessible during Java code generation phase. Line 3 is similar to line 2 with the exception that instead of the terminal rule \code{STRING} the predefined parser rule \code{QualifiedName} is called. \code{QualifiedName} is valid for qualified names such as \textit{net.laaber.mt.Tasks}. Line 4 introduces a new assignment with the operator \code{+=}, which assigns multiple values to the variable \code{rdFacs}. In combination with the \code{+} at the end of the line this assignments means that one or more \code{ResourceDescriptorFactory} rules must be present. Line 5 again is a single value assignment of the custom parser rule \code{SensorFactory}. Finally Line 6 defines that after the previous set up statements 0 or more \code{Task} specifications are required. The asterisk (\code{*}) at the end of the line indicates that 0 or more are expected.

\begin{lstlisting}[caption=Xtext: Set Up Grammar Rule, label=code-grammar-setup]
Domainmodel:
	'hADL' file = STRING
	'className' className = QualifiedName
	rdFacs += ResourceDescriptorFactory+
	sensorFac = SensorFactory
	tasks += Task*
;
\end{lstlisting}

\paragraph{Cross-references} Listing \ref{code-grammar-setup} shows all grammar features used in the \gls{dsl} grammar except cross-references. Usage of cross-references are shown in Listing \ref{code-grammar-cr}. Lines 1 - 3 define the parser rule for \gls{hadl} input parameters, where a new variable (\code{Var}) is created. Lines 5 - 7 show the parser rule for output parameters, where a previously created variable is referenced (\code{[Var]}). Xtext ensures that only variables (\code{Var}) are valid at this point. By default all created variables are allowed to insert there. If only specific variables are desired to be available at that point, a new scope provider for the parser rule \code{Output} must be implemented \seeref{i-scopes}.

\begin{lstlisting}[caption=Xtext: Cross-Reference Example, label=code-grammar-cr]
HadlInput:
	'in' variable = Var ':' type = HadlType
;

Output:
	'out' variable = [Var] 'as' key = OutputKey
;
\end{lstlisting}

For the complete grammar specification see Appendix \nameref{app-grammar-spec}.


\subsection{Java-generation}
\label{i-javagen}
\input{c_implementation/javagen.tex}


\subsection{Validation}
\label{i-val}
\input{c_implementation/validation.tex}

\subsection{Scopes}
\label{i-scopes}
\input{c_implementation/scopes.tex}

\subsection{Content Proposals}
\label{i-auto}
\input{c_implementation/proposals.tex}
