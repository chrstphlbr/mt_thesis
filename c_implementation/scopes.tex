Scoping is strongly connected to cross-references introduced in the Section on grammar above \seeref{i-grammar}. That section gives an example how cross-references are defined in the grammar. The referencing side (e.g. \code{[Var]}) is where scopes come into play. Through ScopeProviders the programmer can programmatically define which elements are valid as cross-reference.

An example is displayed in Listing \ref{code-scoping-example} where the grammar definitions of the \code{link} and \code{unlink} statements are defined. In this example there are three cross-reference \code{[Var]}s defined (in square brackets) and one created \code{Var}. With a ScopeProvider we restrict the first \code{[Var]} on line 2 to a variable referencing an Operational\gls{collaborator}, and the second \code{[Var]} on line 2 to a variable referencing an OperationalObject which is linked in the \gls{hadl} model to the first \code{[Var]}. The \code{[Var]} on line 6 is restricted to a variable referencing an OperationalCollabLink. Such a variable can either be created with a \code{link} statement or passed to the Task as input parameter.

\begin{lstlisting}[caption=Scoping Example, label=code-scoping-example]
Link:
	'link' collaborator = [Var] 'and' object = [Var] 'by' link = QualifiedName 'as' variable = Var
;

Unlink:
	'unlink' link = [Var]
;
\end{lstlisting}

The scope definitions are defined in Xtext in the class \code{net.laaber.mt.scoping.\\DSLScopeProvider}. For the Xtext version used in this project some adaptions are needed in order to get it working. The \code{DSLScopeProvider} has to extend \code{org.eclipse.\\xtext.xbase.scoping.batch.XbaseBatchScopeProvider}. Furthermore the \\\code{XBaseBatchScopeProvider} must be bound to the \gls{dsl}-project's \code{DSLScopeProvider} in the class \code{net.laaber.mt.DSLRuntimeModule}.

Whenever a cross-reference occurs in a \gls{dsl} script the method \code{getScope(EObject context, EReference reference)} of \code{DSLRuntimeModule} is invoked. That method returns an instance of \code{IScope}. Depending on the \code{context} and \code{reference} the ScopeProvider knows for which cross-reference the method was invoked. The implementation then dispatches the calls to methods of the form as seen in Listing \ref{code-scoping-provider-methods}. The naming convention is that it starts with ``scope\_'' followed by the type of the context. The first parameter is the parser rule object and the second parameter is the reference to figure out the position of the cross-reference (compare to Listing \ref{code-scoping-example} line 2).

\begin{lstlisting}[language=Xtend, caption=ScopeProvider Method, label=code-scoping-provider-methods]
def IScope scope_Link(Link l, EReference r)
\end{lstlisting}

The implementation of the ScopeProvider methods relies on the utility class \code{net.laaber.\\mt.DslUtil} and the \code{ModelTypesUtil}. The \code{DslUtil} provides methods to conveniently retrieve variables by different criteria. Those include: (i) simple \gls{hadl} type, (ii) \gls{hadl} List-type, (iii) Java type, and (iv) desired scope. The \code{ModelTypesUtil} is used to perform checks that concern the specified \gls{hadl} model. An example for such a check is given above where the cross-references of the \code{link} statement are described.

A nice side-effect of implementing scopes besides validity is that at the cross-reference positions the \gls{ide} provides content proposals. From a \gls{dsl} script developers perspective scopes act the same as a combination of validation \seeref{i-val} and content proposals \seeref{i-auto} do.