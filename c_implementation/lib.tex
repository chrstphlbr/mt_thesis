This chapter already described how the syntax is defined through grammar, how Java code generation works, how constraints are checked through validators and scopes, how content is proposed (scopes and content proposals). This section discusses how \gls{hadl} primitives \seeref{d-lang-hadl} call the \gls{hadl} runtime framework.

Throughout this thesis it illustrates a couple of Java code examples that include calls of the LinkageConnector and the RuntimeMonitor. The invocation of those method calls with the proper parameters and the handling of the return values is quite cumbersome. Hence the Java code generation would get more complex and congested. All the calls to the \gls{hadl} runtime framework return instances of \code{rx.Observable<T>} as result. \code{rx.Observable<T>} is part of the \gls{rxjava} library \cite{netflix_inc._reactivex/rxjava_????} which enforces an asynchronous programming model. The design of the \gls{dsl} on the other hand is inherently synchronous as statements within a Task may rely on the output of a previous statement. The code generation for asynchronous code would be more complex than synchronous code.

In order to have an \gls{api} that closely resembles the input of the \gls{dsl} and to have synchronous calls into the \gls{hadl} runtime framework, the implementation has a layer between the generated Java code and the \gls{hadl} runtime framework. The library representing that layer is substance of the second project, the \gls{hadl}-lib-project. Figure \ref{fig-dsl-overview} shows an overview of this thesis' work. It shows how the \gls{hadl} synchronous lib fits into the overall picture.

\begin{figure}[t]
	\centering
	\includegraphics[width=\textwidth]{dsl-overview}
	\caption{Overview of this Thesis' Work}
	\label{fig-dsl-overview}
\end{figure}

The \gls{hadl}-lib-project defines its classes and interfaces in the package \code{net.laaber.mt.\\dsl.lib.hadl}. The methods of the contained interfaces and classes are designed to take input parameters that closely resemble the design of the \gls{dsl}. The output parameters are designed in a functional programming style. Two kinds of output parameters are used: (i) methods that have a return value return \code{Either<Throwable, O>}, and (ii) methods that have no return value return \code{Option<Throwable>}. The idea is that error handling is more explicit than doing it with raised exceptions and try-catch blocks. Hence the developer using the library must explicitly check the output of a method if it has returned an error (\code{Throwable}). The following subsections describe the contents of the package in detail. References to the language specification \seeref{d-lang-hadl} are made that point out which parameters of the \gls{hadl} primitives are used for which parameters of the library.

\subsubsection{\code{HadlModelInteractor} and \code{HadlModelInteractorImpl}}
\code{HadlModelInteractor} and \code{HadlModelInteractorImpl} are the interface and implementation for interacting with a \gls{hadl} model file. The provided functionality includes loading a model, storing runtime and executable models, and retrieving a scope of a model by its \gls{id}.

\subsubsection{\code{HadlLinker} and \code{HadlLinkerImpl}} 
\code{HadlLinker} and \code{HadlLinkerImpl} define the functionality which is offered by the LinkageConnector (\code{HADLLinkageConnector} of the \gls{hadl} runtime framework). Listing \ref{code-lib-linker} shows the methods that are available. Line 1 is the set up function which must be called once before any other method is called. 

\paragraph{Acquire and Release} Line 3 acquires a \glspl{hadlel}. \code{archElem} is the \gls{hadl} type (\codeit{hadl\_type} in \ref{code-dsl-ar}) and the ResourceDescriptor (\codeit{rd} in \ref{code-dsl-ar}). Lines 4 - 6 release acquired \glspl{hadlopel}. The parameter to the \code{release} methods is the variable that is referenced in the \code{release} statement of the \gls{dsl} (\codeit{var\_name} in \ref{code-dsl-ar}).

\paragraph{Reference and Dereference} Line 8 creates an OperationalCollabReference between to Operational\glspl{collaborator} (\code{from} and \code{to}) with the \gls{hadl} type \code{refId}. \code{from} and \code{to} must be either an OperationalComponent or an OperationalConnector. \code{from} relates to \codeit{var1}, \code{to} relates to \codeit{var2}, and \code{refId} is \codeit{hadl\_type} in Listing \ref{code-dsl-rd}. Line 9 is the dereference operation that removes an OperationalCollabRef (\codeit{var3} in \ref{code-dsl-rd}). Lines 10 and 11 are the respective reference and dereference operations for OperationalObjects and OperationalObjectRefs.

\paragraph{Link and Unlink} Line 13 creates an OperationalLink between an Operational\gls{collaborator} (\code{collaborator}) and an OperationalObject (\code{object}) through a link (\code{linkId}). \\\code{collaborator} refers to \codeit{var1}, \code{object} refers to \codeit{var2}, and \code{linkId} refers to \codeit{hadl\_type} in Listing \ref{code-dsl-lu}. Line 14 is the unlink operation that removes an OperationalLink (\codeit{var3} in \ref{code-dsl-lu}).

\paragraph{Stop and Start Surrogate scope} Lines 16 stops (\code{stopScope} in \ref{code-dsl-ss}) and line 17 starts (\code{startScope} in \ref{code-dsl-ss}) the Surrogate scope.

\paragraph{Return Values} \code{acquire}, \code{reference} and \code{link} have \code{Either} return vales. If the respective operation was successful the right part of the \code{Either} holds the reference to the resulting operational element. Which is than saved in the variable defined with the \gls{dsl} (after the \code{as} keyword). If the operation was unsuccessful the error which occurred is returned as the left part of the \code{Either} return value. The other operations (\code{setUp}, \code{release}, \code{dereference}, \code{unlink}, \code{startScope} and \code{stopScope}) do not have a specific return value. If the operation was successful \code{Option.none()} is returned, otherwise the occurring error is returned.

\begin{lstlisting}[language=Java, caption=HadlLinker Methods, label=code-lib-linker]
Option<Throwable> setUp(String scopeId);

Either<Throwable, THADLarchElement> acquire(String archElem, TResourceDescriptor resourceDescriptor);
Option<Throwable> release(TOperationalComponent c);
Option<Throwable> release(TOperationalConnector c);
Option<Throwable> release(TOperationalObject o);

Either<Throwable, TOperationalCollabRef> reference(TCollaborator from, TCollaborator to, String refId);
Option<Throwable> dereference(TOperationalCollabRef ref);
Either<Throwable, TOperationalObjectRef> reference(TOperationalObject from, TOperationalObject to, String refId);
Option<Throwable> dereference(TOperationalObjectRef ref);

Either<Throwable, TOperationalCollabLink> link(TCollaborator collaborator, TOperationalObject object, String linkId);
Option<Throwable> unlink(TOperationalCollabLink link);

Option<Throwable> startScope();
Option<Throwable> stopScope();
\end{lstlisting}

\subsubsection{\code{HadlMonitor} and \code{HadlMonitorImpl}}
\code{HadlMonitor} and \code{HadlMonitorImpl} are the interface and implementation that wrap the functionality provided by the RuntimeMonitor (\code{HADLruntimeMonitor} from the \gls{hadl} runtime framework). As already discussed in Section \ref{d-lang-hadl} the load operations from the RuntimeMonitor only supports directly connected elements and uses \code{SensingScope}s to control what elements are loaded from \gls{hadlopel}. The \gls{dsl}'s approach for loading is quite different, as it supports loading of indirectly connected elements (with \code{via} keyword) and only loading of one particular list of \glspl{hadlel} (see Figure \ref{fig-hadl-load-example} and Listings \ref{code-dsl-load-simple} and \ref{code-dsl-load-complex}). Listing \ref{code-lib-monitor} shows the library's methods for loading \glspl{hadlopel}.

There are 2 different kinds of operations: (i) the standard loads which just follow the path and load all the \glspl{hadlopel} that are found; and (ii) the loads which follow the path in the same way but exclude duplicate \glspl{hadlopel}. Duplicate \glspl{hadlopel} are possible when indirect connected elements are loaded. Recall the example from Figure \ref{fig-hadl-load-example}. Suppose the first load (from \textit{hadl\_type} to \textit{hadl\_type1}) returns two distinct \glspl{hadlopel}. The second load (from \textit{hadl\_type1} to \textit{hadl\_type3}) returns one element for each of elements from the first load. It is possible that these two elements from the second load are the same \gls{hadlopel} (refer to the same entity on a collaboration platform). If this is desired behaviour than use the methods on lines 1, 3 and 5, if a mathematical set is desired use the methods on lines 7, 9 and 11. The input parameters of all six methods is equal.

The first parameter (\code{what}) is a tuple (\code{Map.Entry<String, String>}) where the left side denotes the type of the \gls{hadlel} to load and the right side denotes the type of the \gls{hadlcon} that leads to that element (after \gls{dsl}'s \code{load} keyword). The second parameter (\code{startingFrom}) is the \gls{hadlopel} (after \gls{dsl}'s \code{startingFrom} keyword) the load is started from. The last parameter (\code{fromVias}) is a list of tuples that represent the intermediary elements on the path of the load (after \gls{dsl}'s \code{via} keywords). Each tuple of the list is of the same form as the \code{what} parameter.

\begin{lstlisting}[language=Java, caption=HadlMonitor Methods, label=code-lib-monitor, float=t]
Either<Throwable, List<THADLarchElement>> load(Map.Entry<String, String> what, TOperationalComponent startingFrom, List<Map.Entry<String, String>> fromVias);

Either<Throwable, List<THADLarchElement>> load(Map.Entry<String, String> what, TOperationalConnector startingFrom, List<Map.Entry<String, String>> fromVias);
	
Either<Throwable, List<THADLarchElement>> load(Map.Entry<String, String> what, TOperationalObject startingFrom, List<Map.Entry<String, String>> fromVias);

default Either<Throwable, Set<THADLarchElement>> loadWithoutDuplicates(Map.Entry<String, String> what, TOperationalComponent startingFrom, List<Map.Entry<String, String>> fromVias) { ... }

default Either<Throwable, Set<THADLarchElement>> loadWithoutDuplicates(Map.Entry<String, String> what, TOperationalConnector startingFrom, List<Map.Entry<String, String>> fromVias) { ... }

default Either<Throwable, Set<THADLarchElement>> loadWithoutDuplicates(Map.Entry<String, String> what, TOperationalObject startingFrom, List<Map.Entry<String, String>> fromVias) { ... }
\end{lstlisting}

\subsubsection{\code{ProcessScope}}
\code{ProcessScope} is the already introduced \seeref{i-javagen} data object that is passed to Tasks and returned from Tasks, which references the various \gls{hadl} schema/runtime framework objects and objects of this library. Those include the \code{HADLmodel}, \\\code{HADLlinkageConnector}, \code{HADLruntimeMonitor}, \code{HadlLinker} and \code{HadlMonitor} besides others.

\subsubsection{\code{SurrogateStateChangeFailure}}
Section \ref{d-err} describes how the \gls{dsl} handles errors and introduces the two types of errors that can occur. The ones that are exceptional situations and the ones that occur if a Surrogate state change fails. The second type is represented by the class \code{SurrogateStateChangeFailure}. Listing \ref{code-lib-errors} shows a simplified version of the generated Java code for dealing with errors from \gls{hadl} primitives. Line 1 tries to acquire an element. Line 2 immediately checks if an error was returned. If an error was returned line 4 checks whether it is a \code{SurrogateStateChangeFailure}, if so it returns normally from the Task with the error added to the failureList. Any other error triggers a return from the Task by throwing that error.

\begin{lstlisting}[language=Java, caption=Generated Java Error Handling Code, label=code-lib-errors]
Either<Throwable, THADLarchElement> res = linker.acquire(archElem, rd);
if (res.isLeft()) {
	Throwable err = res.left().value();
	if (err instanceof SurrogateStateChangeFailure) {
    		failureList.add((SurrogateStateChangeFailure) err);
    		return res;
    	} else {
    		throw err;
    	}
}
\end{lstlisting}

