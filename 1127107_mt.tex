% Copyright (C) 2014-2015 by Thomas Auzinger <thomas@auzinger.name>

\documentclass[draft, final]{vutinfth} % Remove option 'final' to obtain debug information.

% Load packages to allow in- and output of non-ASCII characters.
\usepackage{lmodern}        % Use an extension of the original Computer Modern font to minimize the use of bitmapped letters.
\usepackage[T1]{fontenc}    % Determines font encoding of the output. Font packages have to be included before this line.
\usepackage[utf8]{inputenc} % Determines encoding of the input. All input files have to use UTF8 encoding.

% Extended LaTeX functionality is enables by including packages with \usepackage{...}.
\usepackage{fixltx2e}   % Provides fixes for several errors in LaTeX2e.
\usepackage{amsmath}    % Extended typesetting of mathematical expression.
\usepackage{amssymb}    % Provides a multitude of mathematical symbols.
\usepackage{mathtools}  % Further extensions of mathematical typesetting.
\usepackage{microtype}  % Small-scale typographic enhancements.
\usepackage{enumitem}   % User control over the layout of lists (itemize, enumerate, description).
\usepackage{multirow}   % Allows table elements to span several rows.
\usepackage{booktabs}   % Improves the typesettings of tables.
\usepackage{subcaption} % Allows the use of subfigures and enables their referencing.
\usepackage[ruled,linesnumbered,algochapter]{algorithm2e} % Enables the writing of pseudo code.
\usepackage[usenames,dvipsnames,table]{xcolor} % Allows the definition and use of colors. This package has to be included before tikz.
\usepackage{nag}       % Issues warnings when best practices in writing LaTeX documents are violated.

% for source code
\usepackage[final]{listings}
% change 'Listing' to 'Code'
%\renewcommand\lstlistingname{Code Segment}
\renewcommand\lstlistlistingname{List of Listings}
\input{additional/lstlisting.tex}

\usepackage[backend=bibtex,style=ieee]{biblatex}
\addbibresource{1127107_mt.bib}

% sideways table
\usepackage{rotating}

\usepackage{tikz}
\usepackage{pgfplots}

\usepackage{pdflscape}

%colours
\definecolor{lightred}{RGB}{255, 207, 204}
\definecolor{lightgreen}{RGB}{206, 255, 204}

\usepackage{hyperref}  % Enables cross linking in the electronic document version. This package has to be included second to last.
\usepackage[acronym,toc]{glossaries} % Enables the generation of glossaries and lists of acronyms. This package has to be included last.
% don't forget to run 'makeglossaries 1127107_mt' in the terminal

% Define convenience functions to use the author name and the thesis title in the PDF document properties.
\newcommand{\authorname}{Christoph Laaber} % The author name without titles.
\newcommand{\thesistitle}{A Domain-Specific Language for Coordinating Collaboration} % The title of the thesis. The English version should be used, if it exists.

% Set PDF document properties
\hypersetup{
    pdfpagelayout   = TwoPageRight,           % How the document is shown in PDF viewers (optional).
    linkbordercolor = {Melon},                % The color of the borders of boxes around crosslinks (optional).
    pdfauthor       = {\authorname},          % The author's name in the document properties (optional).
    pdftitle        = {\thesistitle},         % The document's title in the document properties (optional).
    pdfsubject      = {Subject},              % The document's subject in the document properties (optional).
    pdfkeywords     = {a, list, of, keywords} % The document's keywords in the document properties (optional).
}

\setsecnumdepth{subsection} % Enumerate subsections.

\nonzeroparskip             % Create space between paragraphs (optional).
\setlength{\parindent}{0pt} % Remove paragraph identation (optional).

\makeindex      % Use an optional index.
\makeglossaries % Use an optional glossary.
%\glstocfalse   % Remove the glossaries from the table of contents.

% Set persons with 4 arguments:
%  {title before name}{name}{title after name}{gender}
%  where both titles are optional (i.e. can be given as empty brackets {}).
\setauthor{}{\authorname}{}{male}
\setadvisor{Univ.Prof. Mag.rer.soc.oec. Dr.rer.soc.oec.}{Schahram Dustdar}{}{male}

% For bachelor and master theses:
\setfirstassistant{Univ.Ass. Mag.rer.soc.oec. Dr.techn.}{Christoph Mayr-Dorn}{}{male}

% Required data.
\setaddress{Marchetstraße 19, 2500 Baden}
\setregnumber{1127107}
\setdate{13}{04}{2016}
\settitle{\thesistitle}{\thesistitle} % Sets English and German version of the title (both can be English or German).
%\setsubtitle{Optional Subtitle of the Thesis}{Optionaler Untertitel der Arbeit} % Sets English and German version of the subtitle (both can be English or German).

% Master:
\setthesis{master}
\setmasterdegree{dipl.} % dipl. / rer.nat. / rer.soc.oec. / master

% For bachelor and master:
\setcurriculum{Software Engineering \& Internet Computing}{Software Engineering \& Internet Computing} % Sets the English and German name of the curriculum.

% Define convenience macros.
\newcommand{\todo}[1]{{\color{red}\textbf{TODO: {#1}}}} % Comment for the final version, to raise errors.

% define code command
\newcommand{\code}[1]{\texttt{#1}}
\newcommand{\codeit}[1]{\code{\textit{#1}}}

% define see command
\newcommand{\seeref}[1]{(see \ref{#1})}

% acronyms
\input{additional/acronyms.tex}
\input{additional/glossary.tex}

\begin{document}

\frontmatter % Switches to roman numbering.
% The structure of the thesis has to conform to
%  http://www.informatik.tuwien.ac.at/dekanat

\addtitlepage{naustrian} % German title page (not for dissertations at the PhD School).
\addtitlepage{english} % English title page.
\addstatementpage

\begin{danksagung*}
\input{additional/acknowledgements-german.tex}
\end{danksagung*}

%\begin{acknowledgements*}
%\todo{Enter your text here.}
%\end{acknowledgements*}

\begin{kurzfassung}
\input{abstract/abstract-german.tex}
\end{kurzfassung}

\begin{abstract}
\input{abstract/abstract-english.tex}
\end{abstract}

% Select the language of the thesis, e.g., english or naustrian.
\selectlanguage{english}

% Add a table of contents (toc).
\tableofcontents* % Starred version, i.e., \tableofcontents*, removes the self-entry.

% Switch to arabic numbering and start the enumeration of chapters in the table of content.
\mainmatter

\chapter{Introduction}
\label{c-intro}
\input{c_introduction/introduction.tex}

\chapter{Related Work and Background}
\label{c-barw}
\input{c_background/background.tex}

\chapter{Design}
\label{c-design}
\input{c_design/design.tex}

\chapter{Implementation}
\label{c-impl}
\input{c_implementation/implementation.tex}

\chapter{Evaluation}
\label{c-eval}
\input{c_evaluation/evaluation.tex}

\chapter{Conclusions}
\label{c-con}
\input{c_conclusion/conclusions.tex}

\backmatter

% Use an optional list of figures.
\listoffigures % Starred version, i.e., \listoffigures*, removes the toc entry.

% Use an optional list of tables.
\listoftables % Starred version, i.e., \listoftables*, removes the toc entry.

% code listings
\lstlistoflistings

% Use an optional list of alogrithms.
\listofalgorithms
\addcontentsline{toc}{chapter}{List of Algorithms}

% Add an index.
\printindex

% Add a glossary.
%\printglossary[title=Glossary]
%\printglossary[type=\acronymtype,title=Acronyms]
\printglossaries

% Add a bibliography.
%\bibliographystyle{alpha}
%\bibliography{intro}
\printbibliography

\begin{appendices}
\appendixpage

\section{DSL Grammar Specification}
\label{app-grammar-spec}
\lstinputlisting[nolol=true]{sourcecode/DSL.xtext}

\pagebreak

\section{Scrum hADL Model}
\label{app-hadl-model}
\lstinputlisting[language=hadl, nolol=true]{sourcecode/agile-hadl.xml}


\pagebreak

\section{Scrum DSL Script}
\label{app-dsl-script}
\lstinputlisting[language=dsl, nolol=true]{sourcecode/scrum.col}

\end{appendices}

\end{document}