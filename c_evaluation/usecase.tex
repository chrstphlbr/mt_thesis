This section introduces the use case chosen for evaluating the \gls{dsl}. First it describes the scenario. Second it depicts the collaboration platforms utilised to support the scenario. Then it describes the implementation of the use case. There are three parts that complete the implementation: the \gls{dsl} script, the Surrogates and the Sensors.

\subsection{Scenario}
\label{e-uc-scenario}
The scenario is defined in two steps. On one hand there is the idea what would be a good scenario to test the capabilities of the \gls{dsl}. On the other hand there is the \gls{hadl} model that supports the collaborations of the scenario.

\paragraph{Idea}
Typical workflows often used in software engineering are agile software development methods. Arguably the most popular method today is \gls{scrum}. A full implementation of a \gls{scrum} process as well as a detailed introduction to \gls{scrum}, as described in \cite{rubin_essential_2012}, is out of scope of this thesis.

In \gls{scrum} a product backlog is maintained, which is a prioritised list of features to develop. The actual work is divided into fixed time slots (in a sprint planning meeting), called iterations or sprints, where a group of developers implement the assigned stories or tasks. At the end of each iteration the developers present the implemented features to the rest of the group including the product owner and the \gls{scrum} master. This meeting is called the sprint review. After the sprint review another meeting is held, the sprint retrospective where the team discusses what is going well and what is not concerning the \gls{scrum} process. After that a new sprint starts with a sprint planning meeting. Additionally every day daily stand-up meetings take place, where every developer presents in a couple of words what he or she has been doing since the last daily stand-up and what the next steps are.

This evaluation scenario contains the set up and tear down of two concrete collaborations within the \gls{scrum} process. (i) create chat rooms for developers assigned to a particular story of a sprint at the beginning of the sprint execution phase and remove the chats at the end of the execution phase, and (ii) provide the collaboration infrastructure for a sprint retrospective meeting for all sprint participants, which includes a chat room and a wiki page.

\paragraph{hADL Model}
Figure \ref{fig-scrum-model} shows the \gls{hadl} model required for implementing the scenario. The model is designed with two collaboration platforms in mind. The first one offers functionality for organising \gls{scrum} processes. The second one provides communication and collaboration mechanisms for software development teams. These two platforms and the features used for the scenario are described in Section \ref{e-uc-collabplatforms}.

The model consists of four CollaborationObjects representing a \textit{Sprint}, a \textit{Story}, a \textit{Chat} and a \textit{Wiki}, and two HumanComponents namely \textit{AgileUser} and \textit{DevUser}. The \textit{Sprint} references \textit{Story} with the ObjectReference \textit{containsStory}. This reference is a one-to-many reference indicating that a \textit{Sprint} can contain many \textit{Stories}. A \textit{Story} in turn is linked with \textit{AgileUser} by the Link \textit{responsible}. This Link has many-to-many semantics meaning that a \textit{Story} can have many \textit{responsible} \textit{AgileUsers} assigned. So can a single \textit{AgileUser} be \textit{responsible} for many \textit{Stories}. Every \textit{AgileUser} is related to a single \textit{DevUser} (CollaboratorReference \textit{devUser}) and otherwise round (CollaboratorReference \textit{agileUser}). \textit{DevUser} may be linked to \textit{Chat} with a many-to-many semantics. The Link \textit{invite} indicates that a \textit{DevUser} is invited to a \textit{Chat}. The last CollaborationObject is \textit{Wiki} which has two ObjectReferences to the \textit{previous} and \textit{next} wiki page. The \gls{hadl} types of the \glspl{hadlel} are depicted in the center of the objects.

For the \gls{xml} representation of the \gls{hadl} model visualised in Figure \ref{fig-scrum-model} see Appendix \nameref{app-hadl-model}.

\begin{figure}[t]
	\centering
	\includegraphics[width=\textwidth]{hadl/scrum.png}
	\caption{Scrum hADL Model}
	\label{fig-scrum-model}
\end{figure}





\subsection{Collaboration Platforms}
\label{e-uc-collabplatforms}
In the previous section \seeref{e-uc-scenario} it is mentioned that the \gls{hadl} model is designed with the collaboration platforms in mind. The scenario requires two functionalities: \gls{scrum} process management and collaboration mechanisms for developers. An ideal collaboration platform would support both functionalities and expose an \gls{api} to those. Such a platform does not exist hence the functionalities are split over multiple platforms. \gls{agilefant} is the platform of choice for handling the \gls{scrum} related tasks. \gls{bitbucket} offers an \gls{api} for wiki pages, and \gls{hipchat} provides communication channels.

\subsubsection{\gls{agilefant}} 
\gls{agilefant} is a platform that offers the functionality to manage software development through \gls{scrum}. It provides a web interface and a \gls{rest}-based \gls{api} to access its functionality. The resources of interest to us are backlogs, stories, and users. Backlogs are either products, projects or iterations. Table \ref{tab-agilefant} shows the resources (\glspl{url} and \gls{http} verbs) of the \gls{agilefant} \gls{api} that are of interest to us and their resulting \gls{hadlel} types. The base \gls{url} of the \gls{agilefant} \gls{api} is \url{https://cloud.agilefant.com/<account name>/api/v1/}, where \url{<account name>} is substituted with the actual account name. The \url{{id}} parameter of the \glspl{url} is substituted by the \gls{id} of the desired entity.

\begin{table}
	\centering
	\begin{tabular}{lll}
		\toprule
		\gls{url}					& \acrshort{http} verb			& \glspl{hadlel} types \\
		\midrule
		\url{backlogs/}				& GET					& scrum.obj.Sprint \\ 
		\url{backlogs/{id}/stories}		& GET					& List of scrum.obj.Story \\
		\url{stories/{id}}		 		& GET					& scrum.obj.Story \\
		\url{users/{id}} 				& GET					& scrum.obj. AgileUser \\
		
		\bottomrule
	\end{tabular}
  	\caption{\gls{agilefant} \gls{api} Resources and Corresponding \gls{hadlel} Types}
  	\label{tab-agilefant}
\end{table}

\citeauthor{dorn_christophdorn_????-1} \cite{dorn_christophdorn_????-1} implemented a Java abstraction of the \gls{agilefant} \gls{rest} \gls{api} which is used to implement the \gls{agilefant} Sensors \seeref{e-uc-ss}.


\subsubsection{\gls{bitbucket}}
\gls{bitbucket} is a source code hosting platform with basic means for collaboration. The service run and developed by Atlassian supports users, teams, code repositories, wiki pages, issues and comments for most of those. For the evaluation scenario the user management (\code{scrum.col.DevUser}) and wiki pages (\code{scrum.obj.Wiki}) are of interest. Bitbucket exposes two versions of its \gls{rest} \gls{api} which are both needed for the scenario. Version 1 exposes the wiki resource, which is used for creating and updating wiki pages. Version 2 provides access to the user resource. The base url to both \glspl{api} is \url{https://api.bitbucket.org/{version}/} where \url{{version}} is replaced by \url{1.0} for version 1 and by \url{2.0} for version 2. Version 1 uses basic \gls{http} authentication and version 2 uses OAuth 2.

The \gls{rest} client for \gls{bitbucket} is implemented in the project Atlassian4Hadl \cite{laaber_chrstphlbr_????-2}.

\subsubsection{\gls{hipchat}}
\gls{hipchat} is a communication platform by Atlassian. It offers user management, chat rooms, direct message, groups, notifications and sharing of documents. For the use case scenario the user management (\code{scrum.col.DevUser}) feature and chats (\code{scrum.obj.Chat}) are of interest. \gls{hipchat} offers two \gls{api} versions, but only version 2 is recommended for usage. The base url is \url{https://api.hipchat.com/v2/} and OAuth 2 is used for authentication.

The \gls{rest} client for \gls{hipchat} is implemented in the project Atlassian4Hadl \cite{laaber_chrstphlbr_????-2}.





\subsection{Script Implementation}
\label{e-uc-script}
\input{c_evaluation/scripts.tex}

\subsection{Surrogate and Sensor Implementations}
\label{e-uc-ss}
The very left elements of Figure \ref{fig-dsl-overview} are described in the previous section \seeref{e-uc-script}. This only leaves the very right elements open for discussion. This section describes the implementation of the Surrogates and the Sensors required for the evaluation scenario. Section \ref{br-hadl} introduces the concepts of Surrogates and Sensors. Recall the \gls{scrum} \gls{hadl} model (Figure \ref{fig-scrum-model}) with the scenario (see Sections \ref{e-uc-scenario} and \ref{e-uc-script}) in mind, we conclude that for the \glspl{hadlel} the Surrogate and Sensor implementations in Table \ref{tab-ss-implementations} are necessary. Table \ref{tab-ss-implementations} furthermore shows the project the Surrogates and Sensors implementations are part of. \glspl{hadlel} which do not need a specific Surrogate have either \code{at.ac.tuwien.dsg.hadl.framework.runtime.utils.\\DefaultAcceptingObjectSurrogate} or \code{at.ac.tuwien.dsg.hadl.framework.\\runtime.utils.DefaultAcceptingCollaboratorSurrogate} in the \gls{hadl} model file specified (see Appendix \nameref{app-hadl-model}). For \glspl{hadlel} that do not need a specific Sensor, the SensorFactory does not return a Sensor (\code{null}). Sensors are created at the time when they are needed. 

\begin{table}
	\centering
	\begin{tabular}{llll}
		\toprule
		\gls{hadlel}				& Surrogate	& Sensor	& Project\\
		\midrule
		\code{scrum.obj.Sprint}		& no			& yes	& Agilefant4Hadl \cite{laaber_chrstphlbr_????-1} \\
		\code{scrum.obj.Story}		& no			& yes 	& Agilefant4Hadl \\
		\code{scrum.col.AgileUser}	& no			& yes	& Agilefant4Hadl \\
		\code{scrum.col.DevUser}		& yes		& no		& Atlassian4Hadl \cite{laaber_chrstphlbr_????-2} \\
		\code{scrum.obj.Chat}		& yes		& no		& Atlassian4Hadl \\
		\code{scrum.obj.Wiki}		& yes		& yes	& Atlassian4Hadl \\
		\bottomrule
	\end{tabular}
  	\caption{\gls{hadlel} Surrogate and Sensor Implementations}
  	\label{tab-ss-implementations}
\end{table}

\subsubsection{Surrogates}
This subsection textually describes what functionality the surrogates implement. An in depth description of how to implement Surrogates is out of this thesis' scope. In addition to the tasks described below, every Surrogate handles its own state transitions and the state transitions of the corresponding \gls{hadl}-Operational.

\paragraph{} The \code{scrum.obj.DevUser} Surrogate combines the access to both \gls{bitbucket} and \gls{hipchat}. Therefore a composition ResourceDescriptor is needed. A composition ResourceDescriptor combines multiple ResourceDescriptors into one. This is necessary as the \gls{hadl} LinkageConnector does not support acquiring \glspl{hadlel} with multiple ResourceDescriptors. This composition ResourceDescriptor is represented by the class \code{net.laaber.mt.hadl.atlassian.resourceDescriptor.\\TAtlassianResourceDescriptor}. It consists of ResourceDescriptors for \gls{bitbucket} (\code{TBitbucketResourceDescriptor}) and \gls{hipchat} (\code{THipChatResourceDescriptor}) which are both located in the same package. The Surrogate only checks on acquire if the provided ResourceDescriptor is valid for the collaboration platforms. If not, acquiring fails. The Surrogate is represented by the class \code{net.laaber.mt.hadl.atlassian.\\surrogate.UserSurrogate}.

\paragraph{} The \code{scrum.obj.Chat} Surrogate has more tasks to perform compared to the previous Surrogate. On acquire it checks wether a chat for the provided ResourceDescriptor already exists on the collaboration platform. If existing, it just loads the data, otherwise it creates a new chat. Furthermore it implements the invitation feature, where new users are invited and removed from existing chats. The Surrogate is represented by the class \code{net.laaber.mt.hadl.atlassian.surrogate.ChatSurrogate}.

\paragraph{} The \code{scrum.obj.Wiki} Surrogate loads existing wiki pages or creates new ones otherwise. The \code{scrum.obj.next} and \code{scrum.obj.prev} References are handled by this Surrogate. \gls{bitbucket} does not support saving additional data to wiki pages, hence the information about the previous and next wiki page is stored as first line of a wiki page's content. The Surrogate is represented by the class \code{net.laaber.mt.hadl.atlassian.\\surrogate.WikiSurrogate}.


\subsubsection{Sensors}
Same as with the Surrogates, the Sensors are just textually described. Sensors access the collaboration platforms to retrieve the elements that can be loaded from a \gls{hadlopel} via a particular Link or Reference. From the retrieved data about the elements a Sensor creates the corresponding ResourceDescriptors. The \gls{hadl} runtime framework in turn intercepts the load and creates the \glspl{hadlopel} for the ResourceDescriptors.

\paragraph{} The \code{scrum.obj.Sprint} Sensor loads the stories which are part of a sprint. It is represented by the class \code{net.laaber.mt.hadl.agilefant.sensor.SprintSensor}. 

\paragraph{} The \code{scrum.obj.Story} Sensor loads all responsible \code{scrum.col.AgileUsers} of a story. It is represented by the class \code{net.laaber.mt.hadl.agilefant.sensor.\\StorySensor}. 

\paragraph{} The \code{scrum.col.AgileUser} Sensor loads a single \code{scrum.col.DevUser} that represents the same physical person. The thesis introduces the concept of composition ResourceDescriptors to deal with multiple entities that are represented by a single \gls{hadlopel}. This is the second approach to deal with that case by creating different \glspl{hadlel}, referring to each other and make some assumption how these two elements are equivalent. This use case assumes that a \code{scrum.col.AgileUser} and a \code{scrum.col.DevUser} have the same Email address and therefore are uniquely identifiable on all three collaboration platforms used. This Sensor is represented by the class \code{net.laaber.mt.hadl.agilefant.sensor.AgileUserSensor}.

\paragraph{} The \code{scrum.obj.Wiki} Sensor reads the first line of its own wiki page, retrieves the information about previous and next wiki page and depending on the Reference to follow creates the corresponding ResourceDescriptor. The Sensor is represented by the class \code{net.laaber.mt.hadl.atlassian.sensor.WikiSensor}.

\subsection{Prerequisites}
\label{e-uc-prereq}
The subsections of Section \ref{e-usecase} describe how the evaluation use case is implemented. The following prerequisites concerning the collaboration platforms are necessary for the evaluation program to succeed.

\subsubsection{Set Up Properties}
The \gls{agilefant} \gls{rest} client and Sensors as well as the Atlassian \gls{rest} clients, Surrogates and Sensors need set up in order to work properly. Listing \ref{code-agilefant-properties} shows the \gls{xml} property file necessary for the Agilefant4Hadl project \cite{laaber_chrstphlbr_????-1} to work. Line 4 specifies the account name and lines 5 and 6 specify the login credentials.

\lstinputlisting[language=xml, caption=Agilefant4Hadl Project Properties, label=code-agilefant-properties]{sourcecode/properties-credentials.xml}

Listing \ref{code-atlassian-properties} shows the properties required for the Atlassian4Hadl project \cite{laaber_chrstphlbr_????-2} to work. Lines 4 and 5 specify the \gls{bitbucket} OAuth 2 credentials required for the \gls{bitbucket} \gls{api} version 2. Lines 6 and 7 are the login credentials needed for the \gls{bitbucket} \gls{api} version 1. Lines 8 and 9 define the team name and the repository name in which the wiki pages are created. Only users that are members of the team have access to the wiki pages. Line 10 specifies the OAuth 2 access token for \gls{hipchat}. On line 11 and 12 are the Gmail user name and password for sending Emails. Some features are not exposed through an \gls{api}, therefore an Email is sent for those to manually add the requested feature via the collaboration platform.

\lstinputlisting[language=xml, caption=Atlassian4Hadl Project Properties, label=code-atlassian-properties, float=t]{sourcecode/atlassianCredentials.xml}

\subsubsection{Required Entities}
A couple of entities have to be present on the collaboration platforms for the evaluation program to succeed. The following paragraphs present the required entities by collaboration platform grouped.

\paragraph{\gls{agilefant}} First a product \textit{MT\_Evaluation} must be created. Within this product a project named \textit{MT\_Evaluation\_Product} must exist, which itself contains an iteration named \textit{Sprint1}. \textit{Sprint1} has three stories defined: \textit{Story1}, \textit{Story2} and \textit{Story3}. \textit{Story1} has one responsible user \textit{hadltestuser1}. \textit{Story2} has two responsible users, \textit{hadltestuser1} and \textit{hadltestuser3}. \textit{Story3} has a single responsible user \textit{hadltestuser2}.

\paragraph{\gls{bitbucket}} must have a team created with the same name as specified as \code{bb.teamname} in the Atlassian4Hadl project properties (see Listing \ref{code-atlassian-properties}). That team has a single repository named \textit{MT\_Evaluation}. The repository has three users in the Developer group, \textit{hadltestuser1}, \textit{hadltestuser2} and \textit{hadltestuser3}. These users must have the same Email addresses as the \gls{agilefant} users. The only other requirement is, that after each evaluation script execution the created wiki pages are manually deleted from \gls{bitbucket}.

\paragraph{\gls{hipchat}} only needs three users added to a team. The team name does not matter. The users must have the same Email addresses as the ones on \gls{agilefant} and \gls{bitbucket}.



