The previous two chapters describe how the \gls{dsl} is designed, of which syntactical elements it is comprised of, what behaviour these elements have \seeref{c-design}, and how these features are implemented \seeref{c-impl}. This chapter is about evaluating the results of the work that has been done so far. Recall the overview of this work (see Figure \ref{fig-dsl-overview}): Chapters \ref{c-design} and \ref{c-impl} describe the elements of Xtext \gls{dsl} project (with its Validators, ScopeProviders and ProposalProviders) and the \gls{hadl} synchronous lib. The \gls{hadl} runtime framework is developed by Christoph Mayr-Dorn with some small additions from us that came up during this work. That leaves the very left elements from the overview, the actual scripts written in the \gls{dsl}, and the very right elements that wrap access to the collaboration platforms, the Surrogates and Sensors. In order to evaluate the \gls{dsl} this chapter presents a use case for it, a corresponding \gls{dsl} script which defines the collaborations of the use case, the Surrogates and Sensors, and a simple Java program that brings together the Tasks.

The two aspects that are investigated as part of the evaluation concern the benefits a developer has when using the \gls{dsl} over the \gls{hadl} runtime framework directly from Java. Section \ref{d-approach} introduces the main aspects to use a \gls{dsl}: (i) valid and consistent \gls{hadl} instance modeling, and (ii) \gls{hadl} client code generation. Those aspects are central to the following evaluation.

Section \ref{e-usecase} introduces the use case used to evaluate the \gls{dsl}. Section \ref{e-comparison} compares the \gls{dsl} version vs. the equivalent Java version by looking at the lines of code that are necessary to achieve the same result. Finally Section \ref{e-dsl-destruction} shows ways how a developer can break the \gls{dsl} despite all the checks.

\section{Use Case}
\label{e-usecase}
\input{c_evaluation/usecase.tex}

\section{DSL vs. Java}
\label{e-comparison}
\input{c_evaluation/comparison.tex}

\section{Ways to break the DSL}
\label{e-dsl-destruction}
In the first two section of the evaluation chapter the thesis introduces a scenario, the implementation with the \gls{dsl} and the evaluation in terms of effort reduction. Validity and consistency checks ensure that a \gls{dsl} script is valid. But there are ways to break the \gls{dsl} or the execution despite those checks. This section is about these situations, which can be divided into three areas: (i) create a malicious \gls{dsl} script, (ii) implement incorrect Surrogates and Sensors, and (iii) interference during runtime.

\subsection{\gls{dsl} script}
The Content Proposals, Validators and Scopes, in combination with the checks automatically performed due to the grammar, check many cases in order to get a valid program. There are still some assumptions the implementation makes, that must hold for a valid program.

\paragraph{Incorrect \gls{hadl} model file} The \gls{dsl} checks if a file at the specified location (after \code{hADL} keyword) is readable and therefore exists. The \gls{dsl} does not check whether the provided file is a valid \gls{hadl} model file. If an invalid file is provided the \code{DSLJvmModelInferrer} \seeref{i-javagen} just throws an exception if the \\\code{HadlModelInteractor} \seeref{i-lib} does not return an instance of \code{HADLmodel}.

\paragraph{Invalid ResourceDescriptor factory} After the keyword \\ \code{resourceDescriptorFactory} a ResourceDescriptor factory can be provided. In the \gls{hadl} project exists no notion of ResourceDescriptor factory. The \gls{hadl} synchronous library also does not specify an interface or class that defines a ResourceDescriptor factory. At the time of specification of a ResourceDescriptor factory in the \gls{dsl} script are no checks about the class name provided. Later when a ResourceDescriptor is created (when acquiring an \gls{hadlel}) the used ResourceDescriptor factory's static methods are checked if they return an instance of \code{TResourceDescriptor}. Therefore the implementation assumes that the factory provided after the keyword \code{resourceDescriptorFactory} exposes static methods that return such an instance. The \gls{dsl} checks during compile time whether the defined \code{acquire} statement is valid.

\paragraph{Invalid SensorFactory} For all Loads in a \gls{dsl} script the required Sensors must be provided by the SensorFactory. The \gls{dsl} does not check whether the Loads performed within the script are valid with regard to the SensorFactory. It only checks whether the Loads are valid concerning the \gls{hadl} model.

\paragraph{Invalid \gls{hadlel} - ResourceDescriptor combination} The \code{acquire} statement is checked if the \gls{hadlel} can be acquired and the factory method returns an instance of \code{TResourceDescriptor}. Neither the \gls{dsl} nor the \gls{hadl} runtime framework check if the combination is valid. In the evaluation scenario it is possible to use a ResourceDescriptor for \gls{agilefant} to acquire a \gls{hadlel} that represents an entity on \gls{bitbucket}.

\paragraph{Cyclic Loads} Assume the \gls{hadl} model has two HumanComponents that are referencing each other. Hence it is possible to perform a circular load. If this is done often enough the \gls{dsl} program will crash due to stack overflow as the implementation is based on recursion. The \gls{dsl} assumes that the regular case is that loads are in the order of a couple and therefore a stack overflow should never occur. 



\subsection{Surrogates and Sensors}
The implementations of Surrogates, Sensors and their factories are an ideal point for developers to crash the \gls{dsl}. Especially because the \gls{dsl} relies on sequential execution of the statements of a Task. For example if any method of a Surrogate or Sensor blocks, the whole \gls{dsl} execution will block.

\paragraph{State changes} Surrogates are responsible for handling (changing and reacting to different states) the Operational state and the Surrogate state. If the implementation does not do that properly, the resulting behaviour can be unexpected. Furthermore if an Operational was already released, consecutive start/stop Surrogate scope operations still trigger methods of the Surrogate (\code{asyncBegin}, \code{asyncStop}, \code{asyncLinkTo}, \code{asyncDisconnectFrom} and \code{asyncRelating}).

\paragraph{Factories} Both Surrogate and Sensor factories are provided to the \gls{dsl}. The Surrogate factory is specified within the \gls{hadl} model and the Sensor factory is defined through the \gls{dsl} itself (\code{sensorFactory} keyword). The \gls{hadl} runtime framework and therefore the \gls{dsl} relies on proper implementations of the factories. This means, that for every \gls{collaborator} and CollaborationObject that is acquired a corresponding Surrogate must be returned. Moreover for every \gls{hadlopel} that is used as a starting point of a Load a corresponding Sensor must be returned.

\paragraph{Network connection} As \glspl{hadlopel} have corresponding entities on collaboration platforms it is assumed that a network connection is available. Temporary outages of the network may be handled by the Surrogate/Sensor implementations, but in order to successfully execute collaborations a functional network is required.



\subsection{Runtime}
This section deals with attacks a developer can do during runtime from a Task callers perspective to hinder the execution of the collaborations.

\paragraph{Altered ProcessScope} The \code{ProcessScope} object passed to every Task and returned from every Task \seeref{i-javagen} holds the whole state of the \gls{hadl} runtime. It is assumed that the ProcessScope with all its containing objects are not accessed nor altered from Java code outside of Tasks. Only Tasks are allowed to alter the ProcessScope and therefore the state of the \gls{hadl} runtime.

\paragraph{\gls{hadl} input parameters} The \gls{dsl} assumes that the passed \gls{hadl} input parameters (Operationals) to Tasks are part of the \code{HADLruntimeModel} which is itself part of the ProcessScope. A \gls{hadl} object where this is not the case can not be used with the ProcessScope.

\paragraph{Invalid \gls{hadl} input object states} The Operationals passed as \gls{hadl} input parameters to Tasks are assumed to be in a state that is valid. Validity of the states depends on which operations (\gls{hadl} primitives) are executed on it. Whenever a \gls{hadl} operation is executed with a particular Operational it must be in the correct state, otherwise the program ends with an exception.

\paragraph{Collaboration Platform} The implementation assumes that only the generated code from the \gls{dsl} uses the ProcessScope. In addition to that it assumes that the collaboration structures handled by the \gls{hadl} runtime framework are only altered through the runtime framework as well. Assume we create a chat with the \gls{dsl}, immediately after that delete it through the collaboration platform, and in a consecutive step use the chat with the \gls{dsl} (Link, Reference, etc). Such inconsistencies between the Operational and its corresponding entity on a collaboration platform can only be handled through the Surrogate implementations.
