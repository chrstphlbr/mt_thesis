The previous section \seeref{e-usecase} introduces the use case scenario which is the basis for the evaluation of the \gls{dsl}. This section is about the comparison between the use case implemented with the \gls{dsl} and the Java code that would be necessary to write when using the \gls{hadl} framework \cite{dorn_christophdorn_????} instead. Furthermore it investigates how many mental checks a developer would have to make in order to achieve the consistency provided by the \gls{dsl}. The sections present on a per Task basis (i) the effort a developer saves when using the \gls{dsl} by comparing the \gls{dsl} \gls{loc} versus the generated Java \gls{loc}; and (ii) the consistency checks the \gls{dsl} takes care of. The Java code generation is measured in \gls{loc} (empty lines and comments excluded) and the consistency checks are measured by the amount of checks triggered. Calls to the \gls{hadl} synchronous library are measured as if the library calls were just regular function calls. Only consistency checks that provide added benefits for the developer are counted. The checks that are performed to provide a Java like experience (variable name checks, iterations, uniqueness constraints, lexical scopes) are not measured. Surrogates and Sensors \seeref{e-uc-ss} are excluded from the evaluation as the work for them is identical in both situations, using the \gls{dsl} or Java with the \gls{hadl} runtime framework.

\subsection{Code Effort Reduction}
\label{e-cg}
This section depicts the effort reduction in terms of \gls{loc} between \gls{dsl} and Java. First the fixed effort reduction in form of called library code (\gls{hadl}-lib) and second the actual reduction due to \gls{hadl} client code generation is introduced.

\subsubsection{Fixed Effort Reduction}
This subsection illustrates the fixed reduction of effort a developer has when defining collaborations with the \gls{dsl}, before it presents the effort a developer saves per Task. This reduction is called fixed because it does not depend on the size of the \gls{dsl} script. Table \ref{tab-e-lib} shows the lines of code per \gls{hadl} primitive \seeref{d-lang-hadl} the \gls{hadl} synchronous library \cite{laaber_chrstphlbr_????-4} is accountable for. In total the \gls{hadl} synchronous library decreases the effort for the developer by 511 \gls{loc}.

\begin{table}[h]
	\centering
	\begin{tabular}{lr}
		\toprule
		\gls{hadl} primitive			& \gls{loc} \\
		\midrule
		Acquire					& 45 \\
		Release					& 35 \\
		Reference					& 43 \\
		Dereference				& 39 \\
		Link						& 61 \\
		Unlink					& 37 \\
		Stop Surrogate Scope		& 35 \\
		Start Surrogate Scope		& 35 \\
		Load						& 181 \\
		\midrule
		Total						& 511 \\
		\bottomrule
	\end{tabular}
  	\caption{\gls{hadl} Primitive to \gls{hadl} Runtime Framework through \gls{hadl} Synchronous Library}
  	\label{tab-e-lib}
\end{table}

\subsubsection{Code Generation Reduction}
This section explains the effort reduction due to \gls{hadl} client Java code generation. The data in Table \ref{tab-e-code-generation} is provided on a per task basis. The first column shows the names of the Tasks (or for the first row the set up section). The second row shows the \gls{loc} that are required to implement the use case scenario with the \gls{dsl}. Column 3 lists the \gls{loc} produced by the Java code generator. The first row of the table (apart from the heading) shows the set up section \seeref{e-uc-scripts-su} of the \gls{hadl} runtime framework which takes only 5 \gls{loc} with the \gls{dsl} compared to 151 \gls{loc} in Java. This is mostly due to \gls{di} set up code that is required for the \gls{hadl} runtime framework. The next five rows show the comparison between the \gls{dsl} \gls{loc} and the Java \gls{loc} of the tasks of the evaluation scenario: \textit{Sprint Acquirement}, \textit{Story Chat Creation}, \textit{Story Chats Deletion}, \textit{Sprint Retrospective Set Up}, \textit{Sprint Retrospective Tear Down} described in Section \ref{e-uc-script}. The second to the last row (\textit{Total}) shows the added numbers of \gls{dsl} \gls{loc} and Java \gls{loc}. In the last row the fixed effort reduction is added to the overall Java \gls{loc}.

\begin{table}[h]
	\centering
	\begin{tabular}{lrr}
		\toprule
		Task							& \gls{dsl} [\gls{loc}]	& Java [\gls{loc}]\\
		\midrule
		Set Up Section					& 5		& 151 \\
		Sprint Acquirement				& 6		& 23 \\
		Story Chat Creation				& 19		& 98 \\
		Story Chats Deletion				& 12		& 71 \\
		Sprint Retrospective Set Up		& 24		& 147 \\
		Sprint Retrospective Tear Down	& 10		& 63 \\
		\midrule
		Total							& 76		& 553 \\
		\midrule
		Total + \gls{hadl}-lib				& 76		& 1064 \\
		\bottomrule
	\end{tabular}
  	\caption{Code Generation Effort Reduction}
  	\label{tab-e-code-generation}
\end{table}

From the data of Table \ref{tab-e-code-generation} we conclude, that the effort reduction concerning \gls{loc} decreases in the evaluation scenario by a factor of approximately 7.3. This factor does not include the fixed effort reduction. The \gls{loc} of the fixed effort reduction is constant with respect to the \gls{dsl} script size. Hence the fixed effort reduction is negligible for the factor.

\setlength{\fboxsep}{10pt}
\setlength{\fboxrule}{2pt}
\begin{center}
	\framebox[0.8\textwidth][c]{Effort Reduction Factor: 7.3}
\end{center}

\subsection{Mental Check Effort Reduction}
\label{e-mc}
While writing a \gls{hadl} client in Java, a programmer has to perform numerous mental checks in order to write valid code. These checks concern the consistency of \gls{hadl} clients with respect to correct \gls{hadl} types used with \gls{hadl} primitives \seeref{d-cons}. When specifying collaboration instances with the \gls{dsl}, these checks are automatically performed by the \gls{dsl} compiler. This section highlights how many mental checks a programmer would have to do in the evaluation scenario, if the \gls{hadl} client was written in Java and not with the \gls{dsl}.

Xtext provides three mechanisms to do consistency checks: (i) Content Proposals \seeref{i-auto} provide the developer with choices that are valid at a particular position in the \gls{dsl} script, (ii) Validators \seeref{i-val} are compile-time checks whether a \gls{dsl} statement is valid in its entirety, and (iii) Scopes \seeref{i-scopes} provide suggestions like Content Proposals and additionally do compile-time validity checks like Validators, but only for a single position within a \gls{dsl} statement. All three checks plus the built-in checks that are deduced from the grammar guarantee that a \gls{dsl} script produces valid \gls{hadl} instances. Table \ref{tab-e-cc-dsl} shows how many consistency checks per \gls{dsl} element are performed.

\begin{table}[h]
	\centering
	\begin{tabular}{lrrr}
		\toprule
		\gls{dsl} Language Element				& Content Proposals		& Validators	& Scopes \\
		\midrule
		Acquire								& 1					& 1 			& 0 \\
		Release								& 0					& 0			& 1 \\
		Reference								& 1 					& 1 			& 2 \\
		Dereference							& 0					& 0			& 1 \\
		Link									& 1					& 1 			& 2 \\
		Unlink								& 0					& 0			& 1 \\
		Load									& 1 					& 1 			& 1 \\
		Load (additional per via)					& 1					& 0 			& 0 \\
		\gls{hadl} Input, \gls{hadl} Variable			& 1 					& 0 			& 0 \\
		Assign								& 0					& 0 			& 2 \\
		Add									& 0					& 0			& 2 \\
		Iteration								& 0					& 0 			& 1 \\
		Output								& 0					& 0 			& 1 \\
		\bottomrule
	\end{tabular}
  	\caption{Consistency Checks per \gls{dsl} Language Element}
  	\label{tab-e-cc-dsl}
\end{table}

Based on Table \ref{tab-e-cc-dsl} we calculate the mental effort reduction on a per task basis of the evaluation scenario as shown in Table \ref{tab-e-cc-tasks}. From the figures in the table we conclude that a significant amount of mental work is required from the programmer when writing a \gls{hadl} client in Java. For this work's evaluation scenario the \gls{dsl} performs 70 individual consistency checks. Hence defining collaboration instances with the \gls{dsl} is easier compared to Java.

\begin{table}[h]
	\centering
	\begin{tabular}{l | rrr | r}
		\toprule
		Task							& Content Proposals		& Validators	& Scopes	& Total \\
		\midrule
		Sprint Acquirement				& 1					& 1			& 1		& 3 \\
		Story Chat Creation				& 8					& 4			& 12		& 24 \\
		Story Chats Deletion				& 2					& 0			& 4		& 6 \\
		Sprint Retrospective Set Up		& 11					& 7	 		& 14		& 32 \\
		Sprint Retrospective Tear Down	& 2					& 0			& 3		& 5 \\
		\midrule
		Total							& 24					& 12 			& 34		& 70 \\
		\bottomrule
	\end{tabular}
  	\caption{Consistency Checks per Task}
  	\label{tab-e-cc-tasks}
\end{table}

\begin{center}
	\framebox[0.8\textwidth][c]{Performed Validity/Consistency Checks: 70}
\end{center}
