public void acquireSprint() {
    HadlModel model = loadModel();
    Injector i = setUpInjector(model);
    HADLLinkageConnector lc = setUpLinkageConnector(i, model);

    ModelTypesUtil mtu = i.getInstance(ModelTypesUtil.class);

    THADLarchElement sprintType = mtu.getById("scrum.obj.Sprint");
    // check if sprintType exists

    TAgilefantResourceDescriptor rd = new TAgilefantResourceDescriptor();
    rd.setId("sprint1");
    rd.setName("Sprint1");
    rd.setAgilefantId(156935);

    List<Entry<THADLarchElement, TResourceDescriptor>> mapping = new ArrayList<>();
    mapping.add(new AbstractMap.SimpleEntry<>(sprintType, rd));

    Observable<SurrogateEvent> o = lc.acquireResourcesAsElements(mapping);

    o.subscribe(new Observer<SurrogateEvent>() {
        @Override
        public void onCompleted() {
            // all elements acquired
        }

        @Override
        public void onError(Throwable e) {
            // error while acquiring
            // check what kind of error and handle it
        }

        @Override
        public void onNext(SurrogateEvent t) {
            // element acquired
            // safe element somewhere for later access
        }
    });
}
