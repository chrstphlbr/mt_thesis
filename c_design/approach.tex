This work's \gls{dsl} offers two major aspects which extend the current state of \gls{hadl}. These two purposes are (i) providing means for modeling valid and consistent \gls{hadl} instances, and (ii) generating Java \gls{hadl} client code that interacts with the \gls{hadl} framework. The following subsections discuss the two purposes of the \gls{dsl} below, and show the benefits using it over plain Java by means of examples.

\subsubsection{Valid and consistent \gls{hadl} instance modeling}
In order to achieve valid and consistent \gls{hadl} instances, the \gls{dsl} provides a concise but expressive syntax. The syntax provides \gls{hadl} linkage primitives, a mechanism to load (sense) \glspl{hadlel} from existing \glspl{hadlopel}, and a task abstraction defined by a sequence of \gls{hadl} statements with input and output parameters. All the elements of this feature set are described in detail later in this chapter \seeref{d-lang}. On top of the new syntax, the \gls{dsl} adds constraint and type checks that ensure validity of \gls{dsl}/\gls{hadl} programs \seeref{d-cons}. \gls{ide} features (auto-completion and syntax highlighting) are added such that the production of \gls{dsl} code is more convenient for the developer \seeref{c-impl}. These aspects are completely independent from the actual implementation of \gls{hadl}.

Figure \ref{fig-scrum-sprint-story} shows a simple \gls{hadl} model, where two CollaborationObjects (\code{scrum.obj.\\Sprint} and \code{scrum.obj.Story}) are connected through a ObjectReference (\code{code.obj.\\containsStory}). Assume we want to load all stories of a sprint. Apart from the actual lines of code necessary to write that statement(s), the programmer needs to take care in the Java version that the \glspl{hadlel} are identified with the correct \glspl{id} and that these \glspl{id} are actually valid for the load statement(s). For these checks the \gls{hadl} framework exposes the \code{ModelTypesUtil} class, which provides Java access to the \gls{hadl} model. Hence validity checks of all \gls{hadl} statements are obligatory. Furthermore these checks are performed at runtime which yield in potential errors at runtime when using the Java \gls{api}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{hadl/scrum-sprint-story.png}
	\caption{\gls{hadl} Model Sprint to Story}
	\label{fig-scrum-sprint-story}
\end{figure}

The biggest benefit of the \gls{dsl} is that these validity checks are performed at compile time without additional work to be done by the developer. As checks are done at compile time, the developer is presented with appropriate errors while defining collaboration instances. This removes errors while executing \gls{hadl} collaborations, that are solely due to model to instance incongruities. Hence only valid instance declarations are possible when defining collaborations with the \gls{dsl}. Furthermore the \gls{ide} features offer helpful suggestions to the developer during design time of collaboration instances.





\subsubsection{\gls{hadl} client code generation}
This second aspect includes (i) the actual translation of \gls{dsl} syntax into \gls{hadl} client code (e.g. Java) that calls the \gls{hadl} \gls{api}, (ii) the abstraction mechanism through Java methods, (iii) runtime checks where compile time checks are not possible due to the design of the \gls{hadl} framework (e.g. \gls{hadl} type checks for input parameters), (iv) a runtime error handling system, (v) a way to pass a \gls{hadl} context to and retrieve the context from collaboration abstractions, and (vi) \gls{hadl} set up code.

The main benefit of the client code generation is that the developer of collaboration instances has to do a lot less typing. The example given below demonstrates the effort reduction by the \gls{dsl}. Listing \ref{code-hadl-acquire-sprint} shows the Java version and Listing \ref{code-dsl-acquire-sprint} shows the \gls{dsl} version of the acquirement of a sprint.

Listing \ref{code-hadl-acquire-sprint} shows how a CollaborationObject, here the one that represents the Sprint with the \gls{id} 156935, is acquired. For simplicity reasons the example does not include loading the \gls{hadl}-model (line 2), setting up the \gls{guice} injector and therefore the \gls{hadl} runtime framework (line 3) and initialising the LinkageConnector (line 4). 

\lstinputlisting[language=Java, caption=\gls{hadl} Example for Acquiring a Sprint, label=code-hadl-acquire-sprint]{sourcecode/hadl-acquire-example.java}

The example in Listing \ref{code-hadl-acquire-sprint} clearly indicates that a lot of boilerplate code needs to be written for a trivial task like acquiring an element. Setting up the \gls{hadl} framework, dealing with \gls{hadl} types through the ModelTypesUtil, putting together the required data types (variables mapping and rd), handling errors and using the asynchronous \gls{api} of \gls{rxjava} \cite{netflix_inc._reactivex/rxjava_????} in a callback fashion is a burden for developers.

Listing \ref{code-dsl-acquire-sprint} shows the equivalent (also a bit simplified) \gls{dsl} version of acquiring a sprint opposed to the Java version in Listing \ref{code-hadl-acquire-sprint}. Only three statements are needed to obtain the same effect as in the Java example. (i) Line 1 defines which model should be loaded; (ii) line 5 specifies a factory for obtaining ResourceDescriptors; and (iii) lines 10-12 (split into three lines for improving readability) defines which type with which ResourceDescriptor is acquired and assigned to which variable (\code{s}).

\begin{lstlisting}[language=dsl, caption=\gls{dsl} Version of Acquiring a Sprint, label=code-dsl-acquire-sprint]
hADL "/Users/Christoph/TU/Diplomarbeit/Code/Evaluation/src/main/resources/agile-hadl.xml"

...

resourceDescriptorFactory agilefantRd 
	class net.laaber.mt.hadl.agilefant.resourceDescriptor.AgilefantResourceDescriptorFactory

...

acquire scrum.obj.Sprint
	with agilefantRd.agilefant("Sprint1", 156935)
	as s
\end{lstlisting}
