This section describes what the language elements (syntax) are and how the semantics of those is. First it describes elements that are necessary for set up \seeref{d-lang-gen}. It does not provide information on how the grammar is implemented. This is described in Section \ref{i-grammar}. After that Java types and \gls{hadl} types are discussed \seeref{d-lang-types}. Third the main abstraction mechanism is introduced \seeref{d-lang-tasks}. Following the definition of that abstraction mechanism, the elements for providing input and defining output to it are described \seeref{d-lang-io}. Fifth the \gls{hadl} functionalities exposed by the LinkageConnector and the RuntimeMonitor is shown as \gls{hadl} primitives \seeref{d-lang-hadl}. Section \ref{d-lang-var} describes what kind of variables are available in the \gls{dsl}. And last it gives insight in how iteration is implemented and what it is needed for \seeref{d-lang-it}.

\subsection{Set Up Statements}
\label{d-lang-gen}
Before starting to define collaborations, set up of the \gls{hadl} runtime framework and the \gls{ide} is necessary. Because it is the Design chapter, it just briefly mentions what the \gls{ide} set up is for, but does not go into detail how it is used. This is subject of Chapter \ref{c-impl}. The set up language elements are described below.

\subsubsection{\gls{hadl} Model File}
The \gls{hadl} model is essential for all further features of the \gls{dsl}. These features include: handling \gls{hadl} types through the ModelTypesUtil \seeref{d-cons} and therefore \gls{ide} features such as type checks and auto-completions; and set up code for the \gls{hadl} runtime framework \seeref{c-impl}. As this model is crucial for everything else in the script it is the very first statement \gls{dsl}.

Listing \ref{code-dsl-model-file} shows how the path to the \gls{hadl} model file is defined in the \gls{dsl}. \\ \codeit{path\_to\_hadl\_file} is the absolute file system path in \gls{unix} format \\(e.g. /Users/Christoph/TU/Diplomarbeit/Code/Evaluation/src/main/resources/agile-hadl.xml).

\begin{lstlisting}[language=dsl, deletekeywords={to}, caption=Definition of \gls{hadl} Model File Location, label=code-dsl-model-file]
hADL "path_to_hadl_file"
\end{lstlisting}

\subsubsection{Generated Java File Name}
The next setting is the file name of the generated Java file. The keyword name \code{className} implies that the \gls{dsl} is implemented to produce a single Java file, hence a single Java class \seeref{c-impl}. This set up parameter is mandatory that the \gls{dsl}-to-Java-compiler knows where to create what class. \codeit{fully\_qualified\_name} is a Java class name declaration with fully qualified path (e.g. net.laaber.mt.evaluation.Tasks)  (see Listing \ref{code-dsl-className}).

\begin{lstlisting}[language=dsl, caption=Definition of Generated Java File, label=code-dsl-className]
className fully_qualified_name
\end{lstlisting}

\subsubsection{ResourceDescriptor Factories}
ResourceDescriptors encapsulate information that is needed during runtime to identify a \gls{hadlel} with a resource on a collaboration platform \seeref{br-hadl}. In order to provide the best possible \gls{ux} the \gls{dsl} introduces ResourceDescriptor factories. This statement introduces a new global variable with the name \codeit{var\_name} that references the Java class at \codeit{fully\_qualified\_name} (e.g. net.laaber.mt.hadl.agilefant.\\resourceDescriptor.AgilefantResourceDescriptorFactory) (see Listing \ref{code-dsl-rdfac}). Multiple ResourceDescriptor Factories can be defined.

\begin{lstlisting}[language=dsl, caption=Definition of a ResourceDescriptor Factory, label=code-dsl-rdfac]
resourceDescriptorFactory var_name class fully_qualified_name
\end{lstlisting}

The specified ResourceDescriptor factory is used to create ResourceDescriptors when acquiring a \gls{hadlel} \seeref{d-lang-hadl}. Listing \ref{code-dsl-rd} shows how a ResourceDescriptor factory is used to create a ResourceDescriptor. \codeit{var\_name} is a variable name referring to a ResourceDescriptor factory (e.g. \codeit{var\_name} in Listing \ref{code-dsl-rdfac}). After the variable name a dot is required. Methods of the ResourceDescriptor factory, which return an instance of \code{TResourceDescriptor} (for fully qualified name see Section \ref{br-hadl}), are allowed after the dot (instead of \code{resourceDescriptor}). Then, surrounded by braces, a list of Java-type parameters of arbitrary size are required. The parameters have the same restrictions as described in Section \ref{d-lang-types}.

\begin{lstlisting}[language=dsl, caption=Creation of a ResourceDescriptor, label=code-dsl-rd]
var_name.resourceDescriptor(param1, param2)
\end{lstlisting}

Parameters of type \code{String} can have a value of type \code{int}/\code{Integer} (\codeit{var1}) appended to it (see Listing \ref{code-dsl-rd2}). This feature is of interest when acquiring \glspl{hadlel} within an iteration \seeref{d-lang-it} by using the iteration's counter variable.

\begin{lstlisting}[language=dsl, caption=Appending Numeric Values to ResourceDescriptor String Parameters, label=code-dsl-rd2]
var_name.resourceDescriptor("a String" + var1, param2)
\end{lstlisting}

\subsubsection{Sensor Factory}
Section \ref{br-hadl} introduces two runtime concepts that interact with collaboration platforms: (i) Surrogates and (ii) Sensors. Both are created by registered instances of \code{SurogateFactory} or respectively \code{SensorFactory} (for fully qualified names see \ref{br-hadl}). The runtime framework has a resolver built in, that extracts information about the Surrogate factory from the \gls{hadl} model file. As Sensors are a newer feature of \gls{hadl}, there is no resolver yet available. Hence specification of the Sensor factory with the \gls{dsl} is required. 

Listing \ref{code-dsl-sensorfac} shows the statement to define the location of the Sensor factory. \\\codeit{fully\_qualified\_name} specifies the location of the Java file that contains the Sensor factory class (e.g. net.laaber.mt.evaluation.sensor.DslSensorFactory).

\begin{lstlisting}[language=dsl, caption=Definition of the Sensor Factory, label=code-dsl-sensorfac]
sensorFactory fully_qualified_name
\end{lstlisting}





\subsection{Tasks}
\label{d-lang-tasks}
After having set up the \gls{dsl} and the \gls{hadl} runtime framework, the only other top-level construct that can be specified are tasks. A Task is the logical unit, which is supposed to be called from a process-centric language. Figure \ref{fig-proc-to-hadl} shows an exemplary process with tasks \textit{Process Task A} and \textit{Process Task B} that call the respective  \gls{dsl}'s tasks \textit{\gls{dsl} Task C} and \textit{\gls{dsl} Task D}.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.7\textwidth]{bpmn/process-to-hadl.png}
	\caption{Process-Centric Language to \gls{dsl}}
	\label{fig-proc-to-hadl}
\end{figure}

Listing \ref{code-dsl-task} shows the definition of Tasks (i.e. \textit{DSL Task C} and \textit{DSL Task D}) with the \gls{dsl}. Every Task is identified by its \codeit{task\_name} which must be unique per \gls{dsl} script. The body of a Task, enclosed by curly braces, defines \codeit{inputs} \seeref{d-lang-io}, \codeit{statements} and \codeit{outputs} \seeref{d-lang-io}. The possible \codeit{statements} are \gls{hadl} primitives \seeref{d-lang-hadl}, variables \seeref{d-lang-var} and iterations \seeref{d-lang-it}. At least one \codeit{statement} needs to be present. The execution order of the \codeit{statements} is sequentially.

\begin{lstlisting}[language=dsl, caption=Definition of a Task, label=code-dsl-task, float=t]
task task_name {
	inputs
	statements
	outputs
}
\end{lstlisting}





\subsection{Types}
\label{d-lang-types}
The \gls{dsl} supports two kinds of types: \gls{hadl} types and regular Java types. A simple \gls{hadl} type is specified by a \gls{hadllangel}'s \gls{id}. Listing \ref{code-hadl-component-id} shows the definition of an \gls{id} on a HumanComponent by supplying an id-Tag. The actual \gls{id} is represented by \codeit{hadl\_id}.  CollaborationConnectors, CollaborationObjects, References, Links and Actions also have id-Tags. The \gls{dsl} also supports List-types for \gls{hadl} types, where the \codeit{hadl\_id} is enclosed by square brackets (denoted as \code{[\textit{hadl\_id}]}).

\begin{lstlisting}[language=hadl, caption=\gls{hadl} HumanComponent Tag, label=code-hadl-component-id]
<component id="hadl_id">
	...
</component>
\end{lstlisting}

Listing \ref{code-dsl-types} shows variable declarations with a simple \gls{hadl} type on line 1 and with a list-type on line 2. \codeit{hadl\_type} corresponds to \codeit{hadl\_id} in Listing \ref{code-hadl-component-id}. Section \ref{d-lang-var} gives a detailed explanation on variables.
 
\begin{lstlisting}[language=dsl, caption=Simple and List-Type \gls{hadl} Variables, label=code-dsl-types, float=t]
var var1 : hadl_type
var var2 : [hadl_type]
\end{lstlisting}

Every \gls{hadllangel} has a corresponding Java type, which is described in Section \ref{br-hadl} and is visually presented in Table \ref{tab-element-java-types}.

Java types are supported as input parameters \seeref{d-lang-io}, as parameters to ResourceDescriptor factory calls (see ResourceDescriptor factories in \ref{d-lang-gen}), in the form of iteration counters \seeref{d-lang-it} and as output names \seeref{d-lang-io}. For simplicity reasons the design restricts the set of Java types to \code{String}, \code{int}/\code{Integer}, \code{float}/\code{Float} and \code{double}/\code{Double}.






\subsection{Input and Output}
\label{d-lang-io}
Section \ref{d-lang-tasks} mentions that every task specifies \codeit{inputs} and \codeit{outputs} (see Listing \ref{code-dsl-task}) for providing data to a collaborative task and getting data as a result of a collaborative task. Input parameters can either be a \gls{hadl} type or a Java type. Any variable from the Task can be returned as output parameter.

Listing \ref{code-dsl-input} shows the different ways to specify input parameters to a Task. Line 1 shows the definition of an input parameter of simple \gls{hadl} type \codeit{hadl\_type}, line 2 shows the definition of an input parameter of \gls{hadl} List-type \codeit{hadl\_type}, and line 3 shows the definition of an input parameter of Java type \codeit{java\_type}. \codeit{var1}, \codeit{var2} and \codeit{var3} are placeholders for variable names, and \codeit{java\_type} represents a Java type (for restrictions see \ref{d-lang-types}). Zero or more input parameters are eligible per Task. Input parameters must be the first statements of a Task body.

\begin{lstlisting}[language=dsl, caption=Definition of Input Parameters, label=code-dsl-input]
in var1 : hadl_type
in var2 : [hadl_type]
javaIn var3 : java_type
\end{lstlisting}

As with input parameters, output parameters are optional and must be specified as the last statements of a Task body. \codeit{var1} represents a variable name that is defined within the scope of the Task (more on variable scopes in \ref{d-lang-var}). The value of this variable is returned from the Task, which can be referred to by the caller through \codeit{var2}. \codeit{var2} is called output parameter name, which must be unique within a Task.

\begin{lstlisting}[language=dsl, caption=Definition of Output Parameter, label=code-dsl-output]
out var1 as "var2"
\end{lstlisting}




\subsection{hADL Primitives}
\label{d-lang-hadl}
The \gls{hadl} primitives described in this section include functionality exposed by the LinkageConnector and the RuntimeMonitor of the runtime framework \cite{dorn_christophdorn_????}. These are (i) acquire, (ii) release, (iii) link, (iv) unlink, (v) reference, (vi) dereference, (vii) stopping and (viii) starting of Surrogate scopes, and (ix) loading \glspl{hadlopel} from existing ones.

\subsubsection{Acquire and Release}
Acquire and release are the initial and terminal operations in the lifetime of HumanComponents, CollaborationConnectors and CollaborationObjects \seeref{br-hadl}. Hence only \gls{hadl} types that are HumanComponents, CollaborationConnectors or CollaborationObjects are eligible as \codeit{hadl\_type} in Listing \ref{code-dsl-ar} on line 1. \codeit{rd} on the same line refers to a ResourceDescriptor as created in Listing \ref{code-dsl-rd}. After the \code{as} keyword the name of the variable is specified (\codeit{var\_name}), which holds the result of the acquire statement. Depending on the \gls{hadl} type the result is either an OperationalComponent, OperationalConnector or OperationalObject \seeref{br-hadl}.

Line 3 of Listing \ref{code-dsl-ar} shows how already acquired \glspl{hadlopel} can be released again. Therefore the variable name \codeit{var\_name} needs to refer to an OperationalComponent, OperationalConnector or OperationalObject.

\begin{lstlisting}[language=dsl, caption=Acquire of \glspl{hadlel} and Release of \glspl{hadlopel}, label=code-dsl-ar]
acquire hadl_type with rd as var_name
...
release var_name
\end{lstlisting}


\subsubsection{Link and Unlink}
Link creates a \gls{hadl} link between an Operational\gls{collaborator} and an OperationalObject. Line 1 in Listing \ref{code-dsl-lu} shows the link statement. \codeit{var1} is a variable that references an Operational\gls{collaborator} and \codeit{var2} is one that references an OperationalObject. \codeit{hadl\_type} specifies which Link between \codeit{var1} and \codeit{var2} is used for linking those together. Again the variable name of the result is specified after the \code{as} keyword (\codeit{var3}). The result of a link operation is an OperationalLink.

Unlink removes an existing OperationalLink. Line 3 shows how unlinking is done. \codeit{var3} is the name of an OperationalLink variable.

\begin{lstlisting}[language=dsl, caption=Link and Unlink of \glspl{hadlopel}, label=code-dsl-lu, float=t]
link var1 and var2 by hadl_type as var3
...
unlink var3
\end{lstlisting}

\subsubsection{Reference and Dereference}
Reference is the polymorphic statement for creating both CollaboratorReferences and ObjectReferences. Line 1 in Listing \ref{code-dsl-rd} shows the statement for referencing \glspl{hadlopel}. If \codeit{var1} is an Operational\gls{collaborator}, then \codeit{var2} is an Operational\gls{collaborator} and vice versa. Conversely if \codeit{var1} is an OperationalObject, then \codeit{var2} is an OperationalObject and vice versa as well. Similar to Link, \codeit{hadl\_type} defines the Reference (either \gls{collaborator}Reference or ObjectReference) that is used to relate \codeit{var1} and \codeit{var2}. The result of the reference statement is saved to \codeit{var3}. Depending on whether \codeit{var1} and \codeit{var2} are Operational\glspl{collaborator} or OperationalObjects the result is either an OperationalCollabRef or an OperationalObjectRef.

Line 3 shows the polymorphic dereference statement, which takes as input a variable name (\codeit{var3}) that references either an OperationalCollabRef or an OperationalObjectRef.

\begin{lstlisting}[language=dsl, caption=Reference and Dereference of \glspl{hadlopel}, label=code-dsl-rd]
reference from var1 to var2 with hadl_type as var3
...
dereference var3
\end{lstlisting}

\subsubsection{Stop and Start Surrogate Scope}
Before any changes to Links (link and unlink) and References (reference and dereference) are performed the Surrogate scope must be stopped. To apply the changes made since the last \codeit{stopScope}, \codeit{startScope} must be called. Listing \ref{code-dsl-ss} shows the statements for stopping and starting Surrogate scopes.
 
\begin{lstlisting}[language=dsl, caption=Stopping and Starting of Surrogate Scope, label=code-dsl-ss]
stopScope
...
startScope
\end{lstlisting}

\subsubsection{Load}
All previous statements affected the LinkageConnector of the \gls{hadl} runtime framework \cite{dorn_christophdorn_????}. The load statement is the first and only one that concerns the RuntimeMonitor (as described in Section \ref{br-hadl}). Figure \ref{fig-hadl-load-example} shows a graphical representation of a \gls{hadl} model. It consists of three HumanComponents (white squares) connected through CollaboratorReferences. The left HumanComponent of type \codeit{hadl\_type} is connected through a CollaboratorReference of type \codeit{hadl\_type2} to the middle HumanComponent of type \codeit{hadl\_type1}. Which is in turn connected through a CollaboratorReference of type \codeit{hadl\_type4} to the right HumanComponent of type \codeit{hadl\_type3}.

There are two types of load scenarios. Scenario 1 (see Listing \ref{code-dsl-load-simple}) is when the \gls{hadlopel} to load is directly connected (through a Link or a Reference) to the \gls{hadlopel} we start from (\codeit{var1}). Scenario 2 (see Listing \ref{code-dsl-load-complex}) is when the \gls{hadlopel} to load is indirectly (through other \glspl{hadlel}) connected to the \gls{hadlopel} we start from (\codeit{var1}). In general \codeit{var1} in both scenarios is a variable referring to either an Operational\gls{collaborator} or an OperationalObject. In the example (see Figure \ref{fig-hadl-load-example}) it is an OperationalComponent. The types in the \gls{hadl} model in Figure \ref{fig-hadl-load-example} refer to the same named types in Listings \ref{code-dsl-load-simple} and \ref{code-dsl-load-complex}.

Scenario 1 defines that the starting point (\codeit{var1}) is immediately succeeded by a \code{load} keyword. After \code{load} a construct is inserted, that lists the \gls{hadl} type of the \gls{hadlel} to load (\codeit{hadl\_type1}) separated by a colon (\code{:}) from the \gls{hadl} type of the \gls{hadlcon} (\codeit{hadl\_type2}). The result is a \gls{hadl} List-type value (\codeit{hadl\_type1}) of the loaded \glspl{hadlopel}, which is saved in \codeit{var2}.

\begin{figure}[t]
	\centering
	\includegraphics[width=\textwidth]{hadl/hadl-load-example}
	\caption{hADL Load Example Model}
	\label{fig-hadl-load-example}
\end{figure}

\begin{lstlisting}[language=dsl, caption=Scenario 1: Loading of Directly Connected \glspl{hadlel}, label=code-dsl-load-simple]
startingFrom var1 load hadl_type1:hadl_type2 as var2
\end{lstlisting}

The difference between Scenario 1 and Scenario 2 is that \glspl{hadlopel} are loaded through other \glspl{hadlel}. These load steps between the starting point (\codeit{var1}) and the desired \glspl{hadlopel} (after \code{load} keyword) are defined with the \code{via} keyword. It is eligible to define an arbitrary amount of \code{via} elements. The syntactic construct needed after a \code{via} keyword is identical to the construct after the \code{load} keyword. The result is the same as in Scenario 1, but with \gls{hadl} type \codeit{hadl\_type3}.

\begin{lstlisting}[language=dsl, caption=Scenario 2: Loading of Indirectly Connected \glspl{hadlel}, label=code-dsl-load-complex]
startingFrom var1 via hadl_type1:hadl_type2 load hadl_type3:hadl_type4 as var2
\end{lstlisting}

To process the results of a load statement the iteration statement is used \seeref{d-lang-it}.





\subsection{Variables}
\label{d-lang-var}
Previous Sections introduced ResourceDescriptor factories \seeref{d-lang-gen}, types of the \gls{dsl} \seeref{d-lang-types}, input parameters \seeref{d-lang-io}, and variables created by \gls{hadl} primitives acquire, link, reference and load \seeref{d-lang-hadl}. All of these concepts are either directly related to variables (ResourceDescriptor factories, input parameters and \gls{hadl} primitives), as they create variables, or indirectly related, as all variables have types.

This section describes how we can explicitly declare variables, assign values to it and how these explicit variables differ from input parameters and variables created through \gls{hadl} primitives.

Every variable in the \gls{dsl} has five properties: (i) a name, (ii) a \gls{hadl} type, (iii) a Java type, (iv) an assignment semantics, and (v) a scope availability. First scoping is defined, second the previous mentioned variable declarations are discussed, and third real variables are introduced.

\paragraph{Variable scopes} are a well known concept in programming languages. The \gls{dsl} also supports different variable scopes where a particular variable is visible and accessible. Theoretically with the \gls{dsl} an arbitrary depth of scopes is possible. All variables from outer scopes are accessible from nested scopes. The outer most scope (scope-level 0) is where set up statements are specified. The only variables from this scope are ResourceDescriptor factory variables. The next inner scopes are the ones defined by Tasks (scope-level 1). Every Task defines its own scope, therefore in two different Tasks it is possible to define variables with the same name. On the same scope-level (and enclosing scopes) every variable name must be unique. Within a Task nested scopes are created through Iterations \seeref{d-lang-it}.

\paragraph{ResourceDescriptor factories} create variables that have value semantics. They do not have a \gls{hadl} type, and their Java type is the class they refer to. As they are defined in the outermost scope, they are accessible everywhere.

\paragraph{Input parameters} also have value semantics. If they are defined with the keyword \code{in}, then they have a \gls{hadl} type which is specified after the colon (\code{:}) and are called \gls{hadl} input parameters. The \gls{hadl} type can either be simple or of List-type and the Java type is inferred from the \gls{hadl} type depending on the \gls{hadllangel}. Table \ref{tab-opelement-java-types} in Section \ref{br-hadl} shows which \gls{hadllangel} corresponds to which operational Java type. If the input parameter is defined with the keyword \code{javaIn} then it does not have a \gls{hadl} type und the Java type is specified after the colon (\code{:}). These input parameters are called Java input parameters. See Listing \ref{code-dsl-input} in Section \ref{d-lang-io}.

\paragraph{\gls{hadl} primitives variables} are created through acquire, link, reference and load statements. The assignment semantic is also value semantics.

Acquire's \gls{hadl} type depends on which \gls{hadlel} was acquired (\codeit{hadl\_type} in Listing \ref{code-dsl-ar}), so does the Java type.

Link's \gls{hadl} type depends on the link that was defined to link the Operational\gls{collaborator} and the OperationalObject (\codeit{hadl\_type} in Listing \ref{code-dsl-lu}). Link's Java type is always \code{TOperationalCollabLink}. 

Reference's \gls{hadl} type depends on the reference that was used to relate the Operational\glspl{collaborator} or OperationalObjects (\codeit{hadl\_type} in Listing \ref{code-dsl-rd}). Reference's Java type is \code{TOperationalCollabRef} or \code{TOperationalObjectRef} depending on the \glspl{hadlopel} that are related.

Load's \gls{hadl} type depends on the type of the \gls{hadlel} specified after the \code{load} keyword (\codeit{hadl\_type1} in Listing \ref{code-dsl-load-simple} and \codeit{hadl\_type3} in Listing \ref{code-dsl-load-complex}). As load returns a \gls{hadl} List-type, the corresponding Java type is of \code{List<T>} type, where \code{T} is the corresponding Java type of the loaded \glspl{hadlopel} (see Table \ref{tab-opelement-java-types}).

\paragraph{Real variables} This paragraph introduces the new concept of real variables. All the previous mentioned variables have one thing in common, which is that they all have value semantics. Meaning that the are immutable or do not support destructive assignment. The \gls{dsl} has a feature which supports real variables with destructive assignment semantics.

Real variables are defined similar to \gls{hadl} input parameters, but with the \code{var} instead of the \code{in} keyword. \code{var} is followed by the variable name (\codeit{var1}). Then after a colon (\code{:}) the \gls{hadl} type is specified, which can either be simple or of List-type.

Listing \ref{code-dsl-var} shows on line 1 the declaration of a simple \gls{hadl} variable. To be of use, real variables support not just declaration, but also assignment. Assignment of a real variable is shown on line 3. The value of \codeit{var2} is assigned to the newly created real variable \codeit{var1}. Therefore the \gls{hadl} types of \codeit{var1} and \codeit{var2} must match.

\begin{lstlisting}[language=dsl, caption=Defining and Using Simple Variables, label=code-dsl-var, float=t]
var var1 : hadl_type
...
assign var2 to var1
\end{lstlisting}

Listing \ref{code-dsl-list-var} shows the other option for defining real variables. On line 1 a variable of \gls{hadl} List-type is declared. Reassignments (\code{assign}) of List-type variables is supported. A second operation which adds elements to a List-type variable is exclusive to this variables. Therefore \codeit{var2} must be of \codeit{hadl\_type} from line 1.

\begin{lstlisting}[language=dsl, caption=Defining and Using List-Type Variables, label=code-dsl-list-var]
var var1 : [hadl_type]
...
add var2 to var1
\end{lstlisting}

What do we need real variables for? Recall the definition of variable scopes from above: Variables are only accessible from the same and enclosing scope-levels. When a new scope-level is introduced, through iterations, all variables within this new scope-level are not accessible from the enclosing scope (the Task scope-level). Hence these nested variables are not accessible by \code{out} statements. Real variables come to help by allowing to define a variable on the Task scope-level and assigning (\code{assign}) or adding (\code{add}) a value from an inner scope. For an example see Section \ref{d-lang-it}.





\subsection{Iteration}
\label{d-lang-it}
Iteration is the last statement in this language specification that is introduced. We know from basic programming language courses, that if a list of elements needs processing of each contained element an iteration mechanism is needed. Depending on the programming language paradigm, loops in iterative languages or higher-order functions in functional languages are used to accomplish iteration. 

This \gls{dsl} takes an iterative approach, as the definition of tasks already relies on an iterative approach to which statements it consists of. It is best compared to Java's \textit{enhanced for} statement \cite{oracle_for_????} which is used for iterating over Java Collections and Arrays. The \gls{dsl}'s iteration statement comes in two forms, with and without access to a loop counter variable. 

The simple form without loop counter is shown in Listing \ref{code-dsl-it-simple}. \codeit{var1} represents a previously defined variable of \gls{hadl} List-type \code{[hadl\_type]}. \codeit{var2} represents a different element of the list in every iteration, where the type has to be \code{hadl\_type}. The element's variable (\codeit{var2}) is accessible from within the loop body, which is enclosed by curly braces. The body itself consists again of an arbitrary number of statements. The set of statements include all \gls{hadl} primitives, real variable declarations and usage of those and iterations.

\begin{lstlisting}[language=dsl, caption=Iterating over List-Type Variables, label=code-dsl-it-simple]
for all var1 as var2 {
	statements
}
\end{lstlisting}

The other form where a loop counter variable is defined is shown in Listing \ref{code-dsl-it-counter}. The name of the counter variable (\codeit{var3}) is declared after the keyword \code{counter}. Its Java type is \code{int} and it is accessible within the iteration's body. The rest of the iteration statement is exactly the same as the simple form.

\begin{lstlisting}[language=dsl, caption=Iteration with Loop Counter, label=code-dsl-it-counter]
for all var1 as var2 counter var3 {
	statements
}
\end{lstlisting}

As iterations are part of the statements allowed in iteration bodies, an arbitrary number of nested iterations is possible. Because every iteration statement creates a new scope-level, an arbitrary number of nested scopes can be constructed. Variables (all types) defined within an iteration are not visible to enclosing scopes.

Listing \ref{code-dsl-it-scopes} shows an example where variables defined on an outer scope-level are accessed from inner scope-levels (iteration). In addition it shows the typical usage of loop counter variables.

In order to access those variables created within an iteration we need to declare a real variable on the surrounding scope-level (\code{acquiredElements} on line 1). After a new element is acquired and stored in variable \code{acqEl} on line 3, it is added to the List-type variable \code{acquiredElements} on line 4. Concerning the types for the \code{add} statement: (i) line 1 shows that the \gls{hadl} type of \code{acquiredElements} is \code{[}\codeit{hadl\_type}\code{]} and (ii) line 3 shows that the \gls{hadl} type of \code{acqEl} is \codeit{hadl\_type}. Therefore the \code{add} statement on line 4 is valid. It is possible to e.g. return the acquired elements (\code{acquiredElements}) from the Task through an \code{out} statement.

Line 3 shows how a loop counter variable (\code{i}) is used as input parameter of the ResourceDescriptor factory method. In this way it is possible to acquire elements within iterations and have their ResourceDescriptor properties (e.g. \gls{id} or name) altered.

\begin{lstlisting}[language=dsl, caption=Iteration Example: Nested Scopes and Usage of Loop Counter Variable, label=code-dsl-it-scopes, float=t]
var acquiredElements : [hadl_type]
for all loadedElements as el counter i {
	acquire hadl_type with rd.resourceDescriptor("acq" + i) as acqEl
	add acqEl to acquiredElements
}
\end{lstlisting}
