This last section in the chapter on design of the \gls{dsl} is about errors and how they are treated by the language. There are two types of situations that need special treatment. On one hand there are exceptions, where exceptional situations occur which need treatment by the programmer. Such situations are the ones that indicate bugs in the \gls{dsl} implementation or the \gls{hadl} model, Surrogate or Sensor implementations. On the other hand there are failures that occur during execution of the \gls{hadl} framework, such as Surrogate state change failures. Depending whether the error is an exception or a failure the \gls{dsl} behaves differently. Errors may occur during the execution of \gls{hadl} primitives \seeref{d-lang-hadl}.

\subsubsection{Exceptions} 
Exceptions indicate bugs and therefore the program that calls the Tasks must stop immediately as no further execution makes sense. The programmer has to debug the application and fix the error before the program is restarted.

\subsubsection{Failures}
Failures are the more interesting case of errors as the program, that calls the Tasks, itself can decide which strategy is taken to deal with that failure. For example an acquire statement might fail because the network was down and therefore the collaboration platform could not be reached. A sophisticated program inspects the failure, understands that the failure was caused by unavailability of the network and retries the acquire statement after a few seconds. 

The next paragraphs present a simple failure situation model that only informs the caller of a Task about the occurrence of a failure. A sophisticated failure handling system remains out of scope of this thesis.

A Task is a sequential composition of possibly multiple \gls{hadl} primitives. If some \glspl{hadlel} or \glspl{hadlcon} have been created it is not as simple as just return the failure and let the caller handle it. Handling failure situations requires that the already successfully created elements are returned in addition to the failure, if an out statement for that variable exists.

Listing \ref{code-dsl-fh} shows the complete definition of Task A, where only set up statements are omitted. On lines 3 and 4 two \glspl{hadlel} of the same kind (HumanComponent, CollaborationConnector, CollaborationObject) are acquired. Lines 7 and 8 define that the results of lines 3 and 4 are returned to the caller of Task A. Line 5 creates a reference between the previously acquired elements (\codeit{var1} and \codeit{var2}) and line 9 specifies that the created reference is returned as well.

Assume that the acquire statement on line 4 returns with a failure. In this case it is not possible to just return the failure, but also the already successfully created \codeit{var1} needs to be returned because of line 7.

The solution is that in addition to the explicitly specified output parameters an additional failure list is returned. The caller of a Task needs to check whether the failure list contains any elements. A Task succeeded if the failure list is empty. All specified output parameters that refer to variables that are defined before the failure occurred are returned as well.

In the example from above the output of Task A includes \codeit{var1} (specified on line 7) and the failure list with one element that specifies the failure which occurred during execution of line 5.

\begin{lstlisting}[language=dsl, caption=Failure Handling Scenario, label=code-dsl-fh]
...
task A {
	acquire hadl_type1 with rd.resourceDescriptor("el1") as var1
	acquire hadl_type2 with rd.resourceDescriptor("el2") as var2
	reference from var1 to var2 with hadl_type3 as var3
	
	out var1 as "el1"
	out var2 as "el2"
	out var3 as "l"
}
\end{lstlisting}

\paragraph{Failures in iterations} require enhanced handling. If a failure occurs within an iteration, the failure is added to the failure list and the iteration immediately continues with the next element. After the iteration where the failure occurred iterated through all elements the Task returns. This behaviour applies to nested iterations as well.
