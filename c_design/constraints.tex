Section \seeref{d-approach} presented the main aspects for using the \gls{dsl} over writing \gls{hadl} clients with Java. This section is about how valid and consistent \gls{hadl} collaboration instances are achieved.

In section \ref{d-lang} on language elements remarks are made on conditions that have to be met in order for the \gls{dsl} to work properly. The following sections present all these constraints and Section \ref{i-val} shows how these checks are implemented.

\subsection{Name Checks}
This section describes the names that the \gls{dsl} introduces and how they are checked. There are three kinds of names: (i) variable names, (ii) task names, and (iii) output names.

\paragraph{Variable names} need to be unique in the scope they are in. This is important for two reasons, first that they can be clearly identified when they are referenced and second that variables from inner scope-levels do not shadow over variables with the same name of outer scope-levels. Variable names that need to be unique are ResourceDescriptor variables \seeref{d-lang-gen}, input parameters \seeref{d-lang-io}, variables from \gls{hadl} primitives \seeref{d-lang-hadl}, real variables \seeref{d-lang-var} and iteration variables \seeref{d-lang-it}.

ResourceDescriptor variables are on scope-level 0 and are therefore accessible everywhere in a \gls{dsl} file. Input parameters and variables created directly in a task must be unique on a per Task basis (scope-level 1). For example two different Tasks can both have an input parameter with the name \textit{foo} (see Listing \ref{cc-input-good}), but a single task with an input parameter \textit{foo} and a variable from an acquire statement named \textit{foo} is prohibited (see Listing \ref{cc-input-bad}).

\begin{lstlisting}[language=dsl, caption=Good Example: Input Parameter Names, label=cc-input-good, backgroundcolor=\color{lightgreen}, float=t]
...

task A {
	in param1 : [hadl_type]
	...
}

task B {
	in param1 : [hadl_type]
	...
}
\end{lstlisting}

\begin{lstlisting}[language=dsl, caption=Bad Example: Duplicate Variable Names, label=cc-input-bad, backgroundcolor=\color{lightred}, float=t]
...

task A {
	in var1 : [hadl_type]

	acquire hadl_type2 with rd as var1
	...
}
\end{lstlisting}

The iteration statement introduces one (simple form) or two (form with loop counter variable) new variables itself and possibly contains multiple variables in its body. These need to be unique on the scope-level of the iteration. If the iteration statement itself is on scope-level 1, the variable names must be unique from scope-level 2 (the iteration body) onwards. Two iterations on the same scope-level can have the same element variable name, loop counter variable name and introduce the same variable names in its body (see Listing \ref{cc-it-good}).

\begin{lstlisting}[language=dsl, caption=Good Example: Same Variable Names in Different Scopes, label=cc-it-good, backgroundcolor=\color{lightgreen}]
...

task A {
	...
	for all list as var1 counter i {
		var var2 : hadl_type
		...
	}
	
	for all list as var1 counter i {
		var var2 : hadl_type
		...
	}
	...
}
\end{lstlisting}

Listing \ref{cc-it-bad} shows a bad example, because \codeit{var1} on line 7 is already declared on line 1 and\codeit{var2} on line 8 is already declared on line 7 and line 5.

\begin{lstlisting}[language=dsl, caption=Bad Example: Same Variable Names in Same Scope, label=cc-it-bad, backgroundcolor=\color{lightred}]
...

task A {
	in var1 : hadl_type
	in var2 : hadl_type2
	...
	for all list as var1 counter var2{
		var var2 : hadl_type3
	}
}
\end{lstlisting}

\paragraph{Task names} must be unique on a per file basis. It is possible to define multiple Tasks in one file and every name must be unique. This is necessary for identification purposes \seeref{d-lang-tasks}.

\paragraph{Output names} which are defined after the \code{as} keyword in \code{out} statements \seeref{d-lang-io}, must be unique on a per Task basis. A single task can not have multiple outputs, where the output name is identical, because otherwise the caller of the Task can not differentiate between the different outputs.

\subsection{Type Checks}
The second checks that the \gls{dsl} performs are type checks. As Section \ref{d-lang-types} explains there are two different types that are of interest: (i) \gls{hadl} types that are identified by the id-tag of the \gls{xml} element, and (ii) Java types of the variables. In the following the different type checks executed by the \gls{dsl} compiler are listed.

\subsubsection{\gls{hadl} type checks} 
\gls{hadl} type checks are performed to ensure that statements, which make use of \gls{hadl} types within a Task description, are aware of those types and only allow correctly typed programs. When using the \gls{hadl} runtime framework to construct a \gls{hadl} client, the programmer specifies \gls{hadl} types as Java \code{String}s and has only the type safety provided by the Java type system at compile-time. \gls{hadl} types are not checked until runtime execution of the collaboration.

The thesis already presented the conditions that have to be met for every statement in Section \ref{d-lang}, in order to write \gls{hadl} type correct statements. In the following it showcases the differences between the Java version and the \gls{dsl} version and highlight where type checks are performed in the \gls{dsl} version. Sections \ref{i-val} and \ref{i-scopes} present how these type checks are implemented.

\paragraph{Acquire}
Listing \ref{code-java-tc-acquire} shows what steps are necessary to acquire an element. Compared to the Java version, we can see the \gls{dsl} version in Listing \ref{code-dsl-tc-acquire}. Line 1 in the Java version retrieves the Java object that represents the \gls{hadl} type \code{scrum.obj.Sprint}. The Java version returns a generic \code{THADLarchElement} object and not the object of the most specific type. As the \gls{hadl} type is provided as \code{String}, we can not be sure that this type even exists. 

\lstinputlisting[language=Java, caption=Java Acquire Example, label=code-java-tc-acquire, firstline=8, lastline=19]{sourcecode/hadl-acquire-example.java}

The \gls{dsl} version provides the \gls{hadl} type after the \code{acquire} statement and checks if a \gls{hadllangel} of that type exists and if it is a \gls{hadlel} and therefore qualifies as one that can be acquired. Therefore the \gls{dsl} version is always type correct. If in the Java version the element \code{sprintType} would not exist or is an element that can not be acquired (Links, References, Actions), an error would be sent to the observable \code{o}.

\begin{lstlisting}[language=dsl, caption=\gls{dsl} Acquire Example, label=code-dsl-tc-acquire]
acquire scrum.obj.Sprint with rd.resourceDescriptor("sprint1", "Sprint1", 156935) as var1
\end{lstlisting}

\paragraph{Reference}
Listing \ref{code-java-tc-reference} shows a Java example on how to create a reference \codeit{hadl\_type} between two \glspl{hadlopel} (\codeit{var1} and \codeit{var2}). It specifically shows how two OperationalObjects are referenced. \code{var1} and \code{var2} refers to OperationalObjects and \code{hadl\_type} refers to an OperationalObjectRef. The problem is that the Java version does not have any information about the variables' (\code{var1}, \code{var2} and \code{hadl\_type}) \gls{hadl} types, and if \code{var1} and \code{var2} have a reference \code{hadl\_type} in the \gls{hadl} model connecting them. Listing \ref{code-dsl-tc-reference} shows the \gls{dsl} version of referencing two \glspl{hadlopel}. The \gls{dsl} checks if \codeit{var1} and \codeit{var2} are connectable through a reference of type \codeit{hadl\_type}. Therefore the reference statement is always \gls{hadl} type correct.

\begin{lstlisting}[language=Java, caption=Java Reference Example, label=code-java-tc-reference]
List<Entry<TOperationalObject, TOperationalObject>> fromTo = new ArrayList<>();
fromTo.add(new AbstractMap.SimpleEntry<TOperationalObject, TOperationalObject>(var1, var2);

Observable<SurrogateEvent> o = lc.buildRelations(fromTo , hadl_type, false);
\end{lstlisting}

\begin{lstlisting}[language=dsl, caption=\gls{dsl} Reference Example, label=code-dsl-tc-reference]
reference from var1 to var2 with hadl_type as var3
\end{lstlisting}


\paragraph{Link}
Listing \ref{code-java-tc-link} depicts that Java version of how a link (\code{hadl\_type}) between an Operational\gls{collaborator} (\code{var1}) and an OperationalObject (\code{var2}) is established. Similar to the Java version of the reference statement, there are no compile-time checks if \code{var1} and \code{var2} can be linked with \code{hadl\_type}. The \gls{dsl} (see Listing \ref{code-dsl-tc-link}) adds those compile-time type checks, such that \codeit{var1} and \codeit{var2} can be linked by \codeit{hadl\_type}.

\begin{lstlisting}[language=Java, caption=Java Link Example, label=code-java-tc-link]
Observable<SurrogateEvent> o = lc.wire(var1, var2, hadl_type);
\end{lstlisting}

\begin{lstlisting}[language=dsl, caption=\gls{dsl} Link Example, label=code-dsl-tc-link]
link var1 and var2 by hadl_type as var3
\end{lstlisting}

\paragraph{Load}
Loads with the Java \gls{api} of \gls{hadl} are quite cumbersome to do. Listing \ref{code-java-tc-load} shows a simplified version on how to accomplish that. If only a particular \gls{hadlcon} is of interest to follow for loading \glspl{hadlopel}, this \gls{hadlcon} (\code{hadl\_type2}) has to be added to a \code{SensingScope} (line 1). In a next step the load is executed with the starting \gls{hadlopel} and the previously specified \code{SensingScope}. The loaded elements are then retrieved through the Observable \code{o}. If consecutive loads starting from the loaded elements are desired (via functionality of the load statement of the \gls{dsl}), the programmer must perform a new load from within the Observable with a new \code{SensingScope}. Again the Java version does not do any compile-time checks whether \code{var1} has a \gls{hadlcon} of type \code{hadl\_type2}. These compile-time checks are added by the \gls{dsl}  (see Listing \ref{code-dsl-tc-load}), such that only \codeit{hadl\_type1} and \codeit{hadl\_type2} types are possible if \codeit{var1} has a \gls{hadlel} of type \codeit{hadl\_type1} connected to it through a \gls{hadlcon} of type \codeit{hadl\_type2}.

\begin{lstlisting}[language=Java, caption=Java Link Example, label=code-java-tc-load]
SensingScope s = new SensingScope();
s.getObjectRefs().add(hadl_type2);

Observable<LoadEvent> o = rm.loadFrom(var1, s);
\end{lstlisting}

\begin{lstlisting}[language=dsl, caption=\gls{dsl} Link Example, label=code-dsl-tc-load]
startingFrom var1 load hadl_type1:hadl_type2 as var2
\end{lstlisting}

\paragraph{Variables}
The variables declared with the keyword \code{var} also support \gls{hadl} type checking. Java only knows about operational element types, the \gls{dsl} also is aware of the particular \gls{hadl} type of each variable. For example a Java list of OperationalComponents may contain OperationalComponents of different \gls{hadl} types. A \gls{hadl} List-type variable only contains elements of the particular \gls{hadl} type. The compile-time type checks guarantee that an \code{assign} or \code{add} statement only allows assignment and inclusion of the right \gls{hadl} types.

\subsubsection{Java type checks}
The \gls{dsl} also supports Java type checks besides from \gls{hadl} type checks. It does not add anything to the overall contributions of \gls{hadl}, because the Java version naturally supports Java type checks. Nonetheless the next paragraphs list the Java type checks done in the \gls{dsl}.

The Java type checks of \gls{hadl} variables are implicitly successful, because if the \gls{hadl} types match the Java types must match as well. 

In the set up section in \gls{dsl} scripts we must specify the location of the Sensor factory. The \gls{dsl} checks whether the specified path leads to a Java class that implements the required interface \code{SensorFactory} (for fully qualified path see Section \ref{br-hadl}).

Creation of ResourceDescriptors as part of acquiring new \glspl{hadlel} needs Java type checking as well. The method used for creating a ResourceDescriptor is required to return an instance of \code{TResourceDescriptor} (for fully qualified path see Section \ref{br-hadl}).

The last Java type checks that are performed by the \gls{dsl} are when Java input parameters (\code{javaIn}) are used as parameters to ResourceDescriptor factory methods or as names for output parameters (\code{out}). ResourceDescriptor factory methods support the Java types \code{String}, \code{int}/\code{Integer}, \code{float}/\code{Float} and \code{double}/\code{Double}. Output parameter names must be of Java type \code{String}.
