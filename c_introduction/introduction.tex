This thesis investigates how collaborations are defined through a \gls{dsl}. This \gls{dsl} is based on the \gls{hadl} and enriches the traditional way of describing collaborations with Java and the \gls{hadl} \gls{api} by providing a significantly easier syntax, validity and consistency checks, \gls{ide} features, little set up and an abstraction mechanism that is conveniently usable from Java.

\section{Problem Statement}
\label{intro-problem-stmt}
Since the beginning of software engineering, engineers and researchers put a lot of effort into to build and design software systems. The technical view on these systems dominated as opposed to a more human-centric view. Questions such as \textit{what is the architecture of the software systems?} and \textit{what are the components of the system and how are those connected?} were investigated in more detail than \textit{how do humans use and interact with each other through the software system?}. In human-centric systems questions of the latter type are addressed. In human-centric architecture the design focuses on collaboration and coordination among human workers. Recent research deals with how to build software systems that integrate flexible behaviour of humans by means of software systems.

There are two general paradigms of how to integrate humans into software systems: behaviour-centric and structure-centric approaches \cite{dorn_specifying_2014, dorn_strategies_????, mayr-dorn_framework_2016}. Behaviour-centric approaches put the actual work humans carry out to the center of attention and deal with combination of their results. Examples are process-centric systems \seeref{br-hip}, Mashups which are often strongly connected to process-centric systems \seeref{br-mash} and \gls{cs} \seeref{br-cs}. These approaches focus on \textit{what is done?} where collaboration between humans is either defined by the system or not defined at all.

Structure-centric approaches on the other hand describe how collaboration and coordination is performed between human and/or software-based workers. An example is \gls{hadl} \seeref{br-hadl}. Those focus on \textit{how is it done?}. Hence collaboration/coordination among humans is explicitly defined. 

Either way of describing human architecture has advantages compared to the other. Hence an ideal solution would combine process-centric and structure-centric architecture description languages \cite{dorn_specifying_2014}. \citeauthor{dorn_strategies_????} \cite{dorn_strategies_????} describe three strategies how a combination of them can be modeled. These strategies are either (i) task-driven, (ii) interaction-driven, or (iii) artifact-driven. Apart from specifying these strategies to combine the paradigms, no further research has been published to the best of our knowledge.

\section{Motivating Scenario}
\label{intro-scenario}
Assume a standard agile software development process with \gls{scrum} at a medium sized software development company. Agile software development and therefore \gls{scrum} is an instance of a human-centric system. For example the \gls{scrum} master defines the collaborations of the \gls{scrum} process with the \gls{dsl}. These collaborations support the developers in their process of creating software.

The workload of a specific project is divided into stories. Every week a new development iteration (sprint) starts where open stories are assigned to developers, who implement the stories until the end of the sprint. Multiple developers may be responsible for a single story and many stories may be assigned to a developer. For every story of a sprint a chat room is created and all responsible developers are invited to it, in order to discuss story related topics. At the end of a sprint all of the created chat rooms are deleted and the sprint concludes. After every sprint before the next starts, all developers that took part in the sprint are invited to the sprint retrospective meeting (a chat room). The purpose of this meeting is to discuss the quality of the scrum process and take note of what went well and how possible changes may improve it. These findings are documented on a wiki page.

In this scenario the software company uses a web-based collaboration tool for group communication such as \gls{hipchat}. Sprint retrospective meeting notes are stored in form of wiki pages. As this process is about development of software it is apparent to use the wiki functionality of a source code repository hosting platform like \gls{bitbucket}. Management of the \gls{scrum} process is done through an online tool such as \gls{agilefant}. The important properties of these platforms are, that they have a web \gls{ui} and an \gls{api}. The \gls{ui} for the humans to do the actual work and the \gls{api} to retrieve needed information (sprints, stories and developers) and automatically set up the necessary communication artifacts (chat rooms, wikis).

Recall the above mentioned research deficiencies and the proposed strategies of handling flexible human behaviour in a process-oriented fashion. The interaction-driven strategy \cite{dorn_strategies_????} fits this scenario best as scrum is by design process-centered. Hence we start off by defining the process steps in a process-oriented language choice (e.g. \gls{bpmn} or Little-JIL). Those steps are (i) starting the sprint and creating the chat rooms for each story, (ii) wait for completion of the sprint, (iii) tear down the chat rooms and finish the sprint, and (iv) hold sprint retrospective meeting. For simplicity reasons steps such as story selection and assignment (sprint planning meeting), daily scrum meetings and the sprint review meeting are excluded from the example process.

While the process steps are defined, there is no explicit notion of how collaboration is performed within and across those steps. Therefore a structure-centric language with flexible collaboration descriptions is used. In the next step the collaborations of each step are defined with the structure-centric language of choice (e.g. \gls{hadl}) and executed. If \gls{hadl} is used the collaboration structure is defined as a \gls{hadl} model. Operating on \gls{hadl} structures is done through the \gls{hadl} runtime framework, which is quite verbose, can be error-prone and process developers have to call the appropriate \gls{hadl} \gls{api} calls. 

Those problems are addressed in this work by introducing a \gls{dsl}, which is a concise way of specifying valid \gls{hadl} programs that are easily callable from Java code. Therefore the process developer can focus just on the process itself and has a defined entry point (a method call) to \gls{hadl} code.


\section{Contributions}
\label{intro-contributions}
This thesis defines a \gls{dsl} for specifying valid collaboration instances with \gls{hadl}. Furthermore it aims at narrowing the gap between process-centric languages (e.g. \gls{bpmn} and Little-JIL) and \gls{hadl} in a way that \gls{hadl} programs can easily be defined and used from regular Java code. Furthermore reducing the developer's mental and physical effort is a goal of this thesis. These contributions are addressed by the following research questions:

\newtheorem{researchQuestion}{RQ}
\begin{researchQuestion}
How can we define valid \gls{hadl} programs and what benefits does a programmer gain from such a program? How is validity of a \gls{hadl} program achieved?
\end{researchQuestion}

\begin{researchQuestion}
How can collaborations in \gls{hadl} be abstracted so that they are usable from process-oriented languages with little set up costs?
\end{researchQuestion}

\paragraph{Methodology}
In order to achieve the contributions and provide answers to the research questions asked, this thesis takes the following methodology: First we look at different scenarios and use cases for this work's potential \gls{dsl} and deduct requirements from it and set the scope. The initial scenarios serve as input for the evaluation scenario. The requirements and the scope define the feature set of the \gls{dsl}. An important property of the \gls{dsl} is that the feature set is concise and fully functional as opposed to broad and only operative under special circumstances. Based on the feature set the \gls{dsl} is designed and implemented. The implementation follows the design by adding validity and consistency checks. This \gls{dsl} is then evaluated with the help of a use case scenario. It is measured how much benefit the \gls{dsl} brings compared to using Java with the \gls{hadl} \gls{api} and the results are presented and discussed accordingly.

\section{Thesis Organization}
The remainder of this thesis is structured as follows. 

Chapter \ref{c-barw} provides the background and related work. \gls{hadl} is the basis on which this thesis and its \gls{dsl} build. The concepts that are important for the rest of this thesis are described in detail \seeref{br-hadl}. Furthermore this chapter introduces and compares \gls{hadl} and the \gls{dsl} with approaches in the areas of (i) human involvement processes \seeref{br-hip}, (ii) mashups \seeref{br-mash}, (iii) crowdsourcing \seeref{br-cs} and (iv) software engineering and collaboration/coordination support. 

Chapter \ref{c-design} is about the design of the \gls{dsl}. It introduces the approach to designing the \gls{dsl} and the benefits we desire to achieve with it. Next the language is specified in terms of syntax and semantics. The necessary constraints are discussed which lead to a valid \gls{hadl} program. Finally error handling in the \gls{dsl} is described.

Chapter \ref{c-impl} describes the implementation of the language designed in chapter \ref{c-design}. It starts with an introduction to Xtext/Xtend and how it is used to achieve the design decisions. The other implementation section introduces the \gls{hadl} synchronous library a wrapper for the \gls{hadl} runtime framework. This library provides the functionality of the \gls{hadl} runtime framework in a way that suits the \gls{dsl} more.

Chapter \ref{c-eval} introduces a use case scenario and how it is implemented with the \gls{dsl}. This scenario is then used to evaluate the \gls{dsl}. It shows the effort a developer saves when using the \gls{dsl} compared to Java. This chapter wraps up by describing assumptions that were made and how malicious users can interfere while writing or executing a \gls{dsl} script.

Finally this thesis concludes in Chapter \ref{c-con} by summarising the achievements and give possible directions for future work.