% $Id: INF_Poster.tex 7714 2011-08-31 17:34:46Z tkren $
%
% TU Wien - Faculty of Informatics
% poster template
%
% This template is using the beamer document class and beamerposter package, see
% <http://www.ctan.org/tex-archive/macros/latex/contrib/beamer/>
% <http://www.ctan.org/tex-archive/macros/latex/contrib/beamerposter/>
% <http://www-i6.informatik.rwth-aachen.de/~dreuw/latexbeamerposter.php>
%
% For questions and comments send an email to
% Thomas Krennwallner <tkren@kr.tuwien.ac.at>
%

\documentclass[final,hyperref={pdfpagelabels=true}]{beamer}

\usepackage{TUINFPST}

\title[Software Engineering \& Internet Computing]{A Domain-Specific Language for Coordinating Collaboration}
% if you have a long title looking squeezed on the poster, just force
% some distance:
% \title[Computational Intelligence]{%
%   Integration of Conjunctive Queries over \\[0.2\baselineskip]%
%   Description Logics into HEX-Programs %\\[0.2\baselineskip]%
% }
\author[christoph.laaber@gmail.com]{Christoph Laaber}
\institute[]{%
  Technische Universit{\"a}t Wien\\[0.25\baselineskip]
  Institut f{\"u}r Informationssysteme\\[0.25\baselineskip]
  Arbeitsbereich: Distributed Systems\\[0.25\baselineskip]
  Betreuer: Univ.Prof. Mag. Dr. Schahram Dustdar
  Mitwirkung: Univ.Ass. Mag. Dr. Christoph  Mayr-Dorn
}
\titlegraphic{\includegraphics[height=90mm]{dsgComplet.png}}
\date[\today]{\today}
\subject{epilog}
\keywords{collaboration model, collaboration management, domain-specific language, hADL, software developer support, human-centric system, human-intensive system}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Display a grid to help align images 
%\beamertemplategridbackground[1cm]

% for crop marks, uncomment the following line
%\usepackage[cross,width=88truecm,height=123truecm,center]{crop}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setbeamercolor{block body}{fg=black,bg=white}
\setbeamercolor{block title}{fg=TuWienBlue,bg=white}

\setbeamertemplate{block begin}{
  \vspace*{1cm}
  \begin{beamercolorbox}{block title}%
    \begin{tikzpicture}%
      \node[draw,rectangle,line width=3pt,rounded corners=0pt,inner sep=0pt]{%
        \begin{minipage}[c][2cm]{\linewidth}
          \centering\textbf{\insertblocktitle}
        \end{minipage}
      };
    \end{tikzpicture}%
  \end{beamercolorbox}
  \vspace*{1cm}
  \begin{beamercolorbox}{block body}%
}

\begin{document}

% We have a single poster frame.
\begin{frame}
  \begin{columns}[t]
    % ---------------------------------------------------------%
    % Set up a column
    \begin{column}{.45\textwidth}
    	\begin{block}{Context}
		\begin{itemize}
			\item Software architecture that focusses on human involvement --> puts collaboration into focus
			\item Current state of research: 
			\begin{itemize}
				\item Process-centric approaches: S-BPM, Social BPMN, Little-JIL, WS-HumanTask/BPEL4People 
				\item Structure-centric languages: human Architecture Description Language - hADL
				\item Mashups
				\item Crowdsourcing
			\end{itemize}
			\item Process-centric languages offer better support for executing human workflows than structure-centric languages, but trail behind when specifying collaboration.
			\item hADL: collaboration instance modeling is brittle and verbose
		\end{itemize}	
	\end{block}
      	\begin{block}{Research Questions}
		\begin{itemize}
			\item How can we define valid hADL programs and what benefits does a programmer gain from such a program? How is validity of a hADL program achieved?
			\item How can collaborations in hADL be abstracted so that they are usable from process-oriented languages with little set up costs?
		\end{itemize}

	\end{block}
	\begin{block}{Methodology}
		\begin{itemize}
			\item Requirements and scope analysis of DSL through use cases
			\item Design fully functional syntax and semantics of DSL
			\item Implementation of DSL with Xtext
			\item Evaluation of DSL with Scrum use case scenario
		\end{itemize}
	\end{block}
	\begin{block}{DSL Benefits}
		The DSL brings two huge benefits to developers, compared to writing hADL client code in Java:
		\begin{itemize}
			\item \textbf{Valid and consistence hADL instance modeling}: static hADL type checks --> only valid hADL collaboration instances possible
			\item \textbf{hADL Java client code generation}: concise syntax which generates complex, verbose Java hADL client code
			\item \textbf{Developer support}: IDE suggestions, syntax highlighting, and error feedback
		\end{itemize}
	\end{block}
    	\begin{block}{Implementation}
		\begin{figure}[h]
			\centering
			\includegraphics[width=\textwidth]{figures/dsl-overview.png}
		\end{figure}
		\begin{itemize}
			\item Xtext DSL project: (i) grammar definition, (ii) validity and consistency checks with Validators and ScopeProviders, (iii) IDE suggestions with ProposalProviders
			\item hADL synchronous lib: transforms asynchronous hADL runtime framework into a synchronous API, handles complex input parameters, API close to DSL syntax
			\item Evaluation provides working example of the full stack from DSL script to Surrogates and Sensors
		\end{itemize}
	\end{block}
    \end{column}
    % ---------------------------------------------------------%
    % end the column

    % ---------------------------------------------------------%
    % Set up a column 
    \begin{column}{.45\textwidth}
	\begin{block}{Evaluation}
		The DSL is evaluated with a simplified Scrum process. Below a graphical representation of the hADL model of that process is shown.
		\begin{figure}[h]
			\centering
			\includegraphics[width=\textwidth]{figures/scrum.png}
		\end{figure}
		A DSL script uses the defined hADL model for specifying the collaboration instances.
		\begin{figure}[h]
			\centering
			\includegraphics[width=\textwidth]{figures/hadl-script.png}
		\end{figure}
		Surrogates and Sensors that access the collaboration platforms (Agilefant, Bitbucket, HipChat) are implemented with the hADL runtime framework and registered through the DSL.
	\end{block}
    	\begin{block}{Results}
		The evaluation results show significant decrease in mental and physical work a programmer has to do, when defining collaboration instances with the DSL compared to Java with the hADL runtime framework.
		
		\begin{columns}
			\begin{column}{.45\textwidth}
				\begin{itemize}
					\item Code reduction factor: 7.3
					\item Validity/Consistency checks performed by DSL: 70
				\end{itemize}
			\end{column}
			\begin{column}{.45\textwidth}
				\begin{block}{Download}
					bitbucket.org/chrstphlbr/mt\_dsl
					\begin{figure}[h]
						\centering
						\includegraphics[width=10cm]{figures/dsl-repo-url.png}
					\end{figure}
				\end{block}
			\end{column}
		\end{columns}
	\end{block}  
	\begin{block}{Future Work}
		\begin{itemize}
			\item Increase robustness through more static checks
			\item Sophisticated error handling mechanism
			\item Support extended hADL feature set
			\item Additional syntactical statements
			\item Type safe input and output parameters
			\item Process support
		\end{itemize}
	\end{block}
    \end{column}
    % ---------------------------------------------------------%
    % end the column
  \end{columns}
\end{frame}

\end{document}

%%% Local Variables:
%%% TeX-PDF-mode: t
%%% TeX-debug-bad-boxes: t
%%% TeX-master: t
%%% TeX-parse-self: t
%%% TeX-auto-save: t
%%% reftex-plug-into-AUCTeX: t
%%% End:
