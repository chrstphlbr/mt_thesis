
The \acrlong{hadl} is the basis on top of which this work builds. Introduced by \citeauthor{dorn_architecture-driven_2012} \cite{dorn_architecture-driven_2012} in their work \citetitle{dorn_architecture-driven_2012} \gls{hadl} is a language to specify flexible human behaviour in a structure-centered way \cite{dorn_specifying_2014}.

\gls{hadl} consists of two parts: the language and the runtime framework. The language or schema defines the elements to describe collaborations and coordinations. The runtime framework facilitates instantiation and connection of language elements as well as life cycle handling. A runtime element has a corresponding entity on a particular collaboration platform. Such platforms are for example \gls{bitbucket}, \gls{hipchat} or \gls{agilefant}. The elements and concepts introduced in the following are the ones integral to this thesis. The list is non-exhaustive. Some of the information below can be found in \cite{mayr-dorn_framework_2016}. The rest is based on the source code of \citeauthor{dorn_christophdorn_????}'s \gls{git} repository on \gls{bitbucket} \cite{dorn_christophdorn_????}. 

To get a better understanding of the elements and concepts they are described with help of an exemplary chat room use-case. If possible a reference to the corresponding Java class of the \gls{hadl} project is provided.

\subsubsection{Language Elements}
The core language of \gls{hadl} defines collaborators (HumanComponents and CollaborationConnectors), interactions in form of messages, streams or shared artifacts (CollaborationObjects) and connections (Links and References) between those. The human architecture described through the language elements forms a collaboration pattern or a combination of patterns \cite{dorn_architecture-driven_2012}. Such patterns are identified and described in \cite{dorn_analyzing_2015, dorn_flexible_2012}. \gls{hadl} uses an \gls{xml} representation to represent a human architecture (see Appendix \nameref{app-hadl-model}). HumanComponents, CollaborationConnectors and CollaborationObjects are called \glspl{hadlel}. Links and References are called \glspl{hadlcon}.

Table \ref{tab-element-java-types} shows which language element corresponds to which \gls{xml} tag and Java type. The tags and types are specified in the schema project \cite{dorn_christophdorn_????}. All Java types are classes in the package \code{at.ac.tuwien.dsg.hadl.schema.core.*}.

\begin{table}
	\centering
	\begin{tabular}{lll}
		\toprule
		\gls{hadl} element			& \gls{xml} tag				& Java type \\
		\midrule
		HumanComponent			& \code{<component>}		& \code{THumanComponent} \\
		CollaborationConnector		& \code{<connector>}		& \code{TCollabConnector} \\
		CollaborationObject			& \code{<object>}			& \code{TCollabObject} \\
		Action					& \code{<action>}			& \code{TAction} \\
		Link						& \code{<link>}				& \code{TCollabLink} \\
		CollaboratorReference		& \code{<collabRef>}		& \code{TCollabRef} \\
		ObjectReference			& \code{<objectRef>}		& \code{TObjectRef} \\
		\bottomrule
	\end{tabular}
  	\caption{\glspl{hadlel} and Corresponding \gls{xml} Tags and Java Types}
  	\label{tab-element-java-types}
\end{table}

\paragraph{HumanComponents} are the building blocks that carry out specific tasks, which are essential to the collaboration. They act as the business logic and provide data for the collaboration. HumanComponents can be seen as the "decision makers" in a collaboration. In a chat room regular chat users are considered HumanComponents.

\paragraph{CollaborationConnectors} are secondary or non-essential, replaceable entities that have coordination duties of the collaboration. Efficient and effective interaction between HumanComponents is the main goal of CollaborationConnectors. They can be anything from purely human to fully automated software scripts. A moderator of a chat room would be an instance of a CollaborationConnector.

\paragraph{CollaborationObjects} define the form of interaction among HumanComponents/CollaborationConnectors. Typical examples of interaction forms are Messages, Streams or shared Artifacts, all of which are instances of a CollaborationObject. A description of what Messages, Streams and shared Artifacts are and what differentiates those from each other can be found in \cite{dorn_architecture-driven_2012}. The chat room by itself can be considered as a CollaborationObject of type Stream.

\paragraph{Actions} are specified rights on HumanComponents, CollaborationConnectors and CollaborationObjects. There are two kinds of Actions: HumanActions and ObjectActions. HumanActions are access rights that \glspl{collaborator} require in order to fulfill their roles. ObjectActions are rights that CollaborationConnectors expose for \glspl{collaborator} to use. Actions can be seen as connection points. HumanActions are connected to ObjectActions through Links, which are described below. Multiple connections to a single Action are possible. A HumanAction on a chat user could be \textit{ChatroomInvite} and an ObjectAction on the chat room could be \textit{Invite}.

\paragraph{Links} connect \glspl{collaborator} with CollaborationObjects or more precisely HumanActions with ObjectActions. A link that connects the HumanAction \textit{PostChatroomMessage} with the ObjectAction \textit{Post} could be named \textit{postToChatroom}.

\paragraph{References} are relations such as inheritance, composition or containment between either Collaborators or CollaborationObjects. Relations between Collaborators are called CollaboratorReferences. Relations between CollaborationObjects are called ObjectReferences. An administrator HumanComponent could have an inheritance relation with a regular chat user HumanComponent.



\subsubsection{Runtime Elements and Concepts} The previous section describes the Language Elements of \gls{hadl}. This section introduces the elements and concepts that are part of the \gls{hadl}-Runtime. A \gls{hadl}-Client makes use of the language elements defined by the \gls{hadl}-model in \gls{xml} form (see Appendix \nameref{app-hadl-model}), and instantiates, connects, loads and releases instances of those. 

\paragraph{LinkageConnector} The LinkageConnector provides mechanisms to acquire, link, reference, unlink, dereference and release \gls{hadl} language elements. Furthermore it starts and stops the scope of Operationals, which are described below.\\
Java class: \code{at.ac.tuwien.dsg.hadl.framework.runtime.impl.\\HADLLinkageConnector}

\paragraph{RuntimeMonitor} The RuntimeMonitor is responsible for loading (sensing) Operationals from existing Operationals via Links or References. This is done by Sensors, which are described below.\\
Java class: \code{at.ac.tuwien.dsg.hadl.framework.runtime.impl.\\HADLruntimeMonitor}

\paragraph{Operationals} are the runtime equivalent of the language elements. Hence for every language element an Operational exists. A \gls{hadl}-Client can retrieve Operationals either through the LinkageConnector or the RuntimeMonitor. Operational\glspl{collaborator} (OperationalComponents and OperationalConnectors) and OperationalObjects are either acquired (LinkageConnector) or loaded (RuntimeMonitor) from existing Operationals. Operationals for Links (OperationalLink) are returned by the LinkageConnector when an Operational\gls{collaborator} is linked with an OperationalObject. Operationals for References are returned when either Operational\glspl{collaborator} (OperationalCollabRef) or OperationalObjects (OperationalObjectRef) are referenced.

Operationals of \glspl{collaborator} and CollaborationObjects always have a corresponding Resource Descriptor (described below). All Operationals are in a particular state at any time and keep a reference to a Surrogate (described below). The different states are described in \cite{mayr-dorn_framework_2016}.

Table \ref{tab-opelement-java-types} shows the \glspl{hadlel} with their corresponding \gls{hadlopel} and Java types. All Java types are in the package \code{at.ac.tuwien.dsg.schema.\\runtime.*}.

\begin{table}
	\centering
	\begin{tabular}{lll}
		\toprule
		\gls{hadlel}			& \gls{hadlopel}			& Java type \\
		\midrule
		HumanComponent		& OperationalComponent	& \code{TOperationalComponent} \\
		CollaborationConnector	& OperationalConnector	& \code{TOperationalConnector} \\
		CollaborationObject		& OperationalObject		& \code{TOperationalObject} \\
		Action				& OperationalAction		& \code{TOperationalAction}	 \\
		Link					& OperationalLink			& \code{TOperationalCollabLink}	\\
		CollaboratorReference	& OperationalCollabRef	& \code{TOperationalCollabRef} \\
		ObjectReference		& OperationalObjectRef	& \code{TOperationalObjectRef} \\
		\bottomrule
	\end{tabular}
  	\caption{\glspl{hadlel} and Corresponding \glspl{hadlopel} and Java Types}
  	\label{tab-opelement-java-types}
\end{table}

\paragraph{ResourceDescriptors} encapsulate information that is necessary for Surrogates and Sensors to relate to corresponding elements on a collaboration platform. A Resource Descriptor needs to be supplied to the LinkageConnector when acquiring a \gls{hadlel}. A ResourceDescriptor can be any class implementing \\
\code{at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor}.

\paragraph{Surrogates} are proxy objects that are triggered by the \gls{hadl} runtime framework upon acquire, link, reference, unlink, dereference and release as well as starting and stopping the Surrogate scope through the LinkageConnector. Their task is to map changes of the \gls{hadl} runtime model to changes on the respective collaboration platforms. For example a Surrogate for a chat room calls the \gls{api} of the collaboration platform (e.g. \gls{hipchat}) when a chat user is invited to a chat room. An invitation can be modeled as a link between HumanAction \textit{ChatroomInvite} and ObjectAction \textit{Invite}.

A Surrogate is at any time in one of the following states described in \cite{mayr-dorn_framework_2016}. Starting and stopping the Surrogate Scope (LinkageConnector) transitions the states of all Surrogates. This is done after an acquired element is changed (link, unlink, reference, dereference). Changes are applied to the collaboration platform on starting the Surrogate Scope.

Surrogates are classes that implement either \code{at.ac.tuwien.dsg.hadl.\\framework.runtime.intf.IObjectSurrogate} or \code{at.ac.tuwien.dsg.hadl.\\framework.runtime.intf.ICollaboratorSurrogate}.\\
The framework needs to know about an instance of \code{at.ac.tuwien.dsg.hadl.\\framework.runtime.intf.SurrogateFactory} which can be either specified within the \gls{hadl} \gls{xml} or provided programmatically (via \gls{di}).

\paragraph{Sensors} are used in combination with the previous introduced RuntimeMonitor to load Operationals from an existing Operational. Therefor the implementation of a sensor accesses the collaboration platform to get the connected elements, creates respective Resource Descriptors and emits a LoadEvent. The RuntimeMonitor intercepts emission and acquires the Operationals for the LoadEvent. With  SensingScopes the \gls{hadl}-Client can limit the Links, References or Actions that are being considered for loads.

Sensors are classes that implement either \code{at.ac.tuwien.dsg.hadl.framework.\\runtime.events.ICollaboratorSensor} or \code{at.ac.tuwien.dsg.hadl.\\framework.runtime.events.ICollabObjectSensor}.\\
Sensors are created by the framework with a registered instance (via \gls{di}) of \code{at.ac.\\tuwien.dsg.hadl.framework.runtime.events.SensorFactory}.


\subsubsection{Process Support}
\label{br-hadl-ps}
\gls{hadl} itself supports a model-driven approach to collaborations, but lacks the possibility to run collaborations in a process-centric fashion. In \cite{dorn_flexible_2012, dorn_specifying_2014, dorn_strategies_????} a concept is introduced to combine process-centric languages such as Little-JIL or \gls{bpmn} with structure-centric collaboration languages like \gls{hadl}. This thesis' work narrows the gap between those two by introducing a \gls{dsl} that creates functions containing sequential \gls{hadl} statements to be called by tasks/steps of a process. The process of design \seeref{c-design}, implementation \seeref{c-impl} and evaluation \seeref{c-eval} of the \gls{dsl} is the main part of this thesis.
