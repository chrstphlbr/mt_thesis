This first related work section deals with process-centric approaches. In the following \gls{sbpm}, Social \gls{bpm}, Little-JIL and \gls{ws-ht}/BPEL4People are investigated and compared to \gls{hadl}/this thesis' approach.

A business process is a set of interrelated tasks/steps handled by workers (human, computational or mixed) in a logical (with respect to the business) and chronological order. In each task a worker uses resources (information) to reach the goal of the business process and therefore satisfy the customer. Additionally every business process has a defined start state, input data and an end state with a result \cite{fleischmann_subject-oriented_2012}.

\subsubsection{\acrlong{sbpm} - \acrshort{sbpm}}
\gls{bpm} is important for the success of organizations. With \gls{bpm} organizational strategies and business models are implemented in a process-centric way. Opposed to \gls{bpm} \gls{sbpm} brings the subjects (actors) who carry out the actual work to the center of attention. In \gls{sbpm} there are four roles: (i) Governors act as bridges between executives and operational business. They take responsibility and assure the quality of the process; (ii) Actors do the actual work in business processes. They are supported by Experts and Facilitators; (iii) Experts are specialist in a particular field and are triggered by any other role; and (iv) Facilitators support Actors in organizational development to satisfy stakeholder's needs  \cite{fleischmann_subject-oriented_2012}.

In a first step the subjects of business processes are identified and described with a collection of \glspl{sbd}. Next the communication among those need to be represented (subject-to-subject communication). Communication in \gls{sbpm} is done through messages which optionally contain business objects (structured information). As a result we get a \gls{sid} or alternatively called \gls{csd}. Messages between subjects are either asynchronously or synchronously exchanged, hence the subjects do their work autonomously and in parallel, whereas traditional \glspl{bpm} assumes a central control flow \cite{fleischmann_subject-oriented_2012, fleischmann_subject-oriented_2013}. \citeauthor{fleischmann_subject-oriented_2012} \cite{fleischmann_subject-oriented_2012} furthermore describe how to model \gls{sbpm} with subprocesses and other complex process networks, subject and their behaviour's description including exception handling, validation of processes and models and optimization aspects. 

As business processes are mostly described in natural language \cite{fleischmann_subject-oriented_2012} by non-technical personnel it takes another level of indirection to bring those informal definitions into correct \gls{sbpm} models. In \cite{oppl_articulation_2015} an approach based on physical card boards is introduced to create \gls{sbpm} models. This method generates semantically incomplete models such that simulation and refinement of the model is applied as last step. This yields semantically and syntactically correct \gls{sbpm} models.

In \cite{fleischmann_subject-oriented_2012} it is described how to implement \gls{sbpm} processes. \citeauthor{ras_s-bpm_2013} \cite{ras_s-bpm_2013} show how a \gls{sbpm} process can be executed using \gls{wf}.

In traditional \gls{sbpm} subjects are always represented by a single entity (human, computational or mixed). \citeauthor{fleischmann_subject-oriented_2013} \cite{fleischmann_subject-oriented_2013} propose a system that unifies multi agent-systems with the \gls{agr} model and \gls{sbpm}. The benefit is that the same business process can be implemented using different organizational structures. Reversely agents or agent structures can implement different processes. Hence we gain a more variable and flexible system where reusability of processes and agents is possible.

\citeauthor{krauthausen_engines_2015} \cite{krauthausen_engines_2015} take a different approach to subject-orientation in business processes than \gls{sbpm}. Relying on baskets that act as a collection of business objects (input/result of work) with publish and subscribe semantics on them for either business tasks or step tasks. Business objects are routed to business tasks through the Subject Communication Engine. A business task is always associated with a human worker and can be divided into sub tasks and action items that are carried out by a worker. The result of an action item is then published to a basket by the worker. On the other hand business objects are assigned to step tasks by the Subject Task Execution Engine. A step task is a software automated function consuming a business object and publishing the result to a basket. 

\gls{sbpm} is conceptually based on process algebras like \gls{ccs} and \gls{csp} where communication between subjects has message-based semantics \cite{fleischmann_subject-oriented_2012}. \citeauthor{krauthausen_engines_2015}'s \cite{krauthausen_engines_2015} approach uses publish/subscribe-based communication. Compared to those two \gls{hadl} does not have any restrictions on how communication between humans is realised. The way of communication is only dependent on the implementation of the respective CollaborationObject.




\subsubsection{Social \gls{bpm}}
Social \gls{bpm} combines \gls{bpm} with social software such as Wikipedia or social networks, changing the traditional approach from closed to an open/social one. This fusion can be applied to design and/or enactment phase.

\citeauthor{dengler_social_2010} \cite{dengler_social_2010} propose a system where Wikis are used to textually describe business processes and keep them in sync with the process model and find/coordinate collaborators on social networks. Wikis in this approach are based on \gls{smw} which uses semantic annotations for connecting Wikis. Each Wiki represents an activity in the process. In an export step Wikis are transformed with the \gls{rdf} into Petri Nets which are then executed by a process execution engine. Coordination and communication in social networks are handled by a model called Community Process. In Community Process there are three different stages: finding partners, building relationships and executing collaboration. Each Community Process has a set of Community Process Objects (Users and Data) which are transferred between activities. An extension based on case-based reasoning integrates Community Process with services of business information systems. Changes to the process model are exported back to the Wiki. Therefore the Wiki and the process model are kept in sync.

In \cite{brambilla_bpmn_2011} \cite{brambilla_combining_2012} \gls{bpmn} is extended to support social interactions through patterns. The following social tasks are defined by this extension: social broadcast, social posting, invitation to activity, commenting, voting, login to join, invitation to join a network and search for actors's information. These tasks are performed by either a single or multiple users. Furthermore social design patterns are defined by the extension which can be assembled to form social processes. The introduced solution is implemented in WebRatio that transforms \gls{bpmn} models via WebML into JavaEE applications.

PROWIT \cite{schwantzer_prowit:_2011} implements a user-centric process system based on Liferay portals. All collaborations are done through a single platform. A \gls{bpm} model is loaded into the platform, user tasks are created and assigned to the specified roles. Users of the platform get tasks for their roles presented in a portal.

All social \gls{bpm} solutions presented provide means for collaboration between humans through a social platform. The differences to \gls{hadl} are that collaborations are only possible within a single task \cite{brambilla_bpmn_2011} \cite{brambilla_combining_2012} and/or collaboration is used for designing a process \cite{dengler_social_2010}. PROWIT \cite{schwantzer_prowit:_2011} is a social platform for executing processes, whereas \gls{hadl} does not make any assumption on which tools are used for communication.




\subsubsection{Little-JIL}
Little-JIL is a graphical process-centric language for defining collaborations. The main abstraction is a step, which is similar to a task in \gls{bpmn}. A Little-JIL program is a tree of steps where the leaf steps represent the smallest work loads and the structure of the tree indicates how coordination is done. Each step has exactly one agent associated with it. An agent can be either human or computational. A step can be in many states of which the most important are: posted to an agent, started by agent, successfully completed or terminated with exception. Figure \ref{fig-little-jil-step} shows a graphical representation of a step with all possible badges and connections to other steps. Steps always have at most one parent step and 0 or more sub-steps. All non-leaf steps can have one of the following control flow kinds: sequential, parallel, try, choice. The control flow defines how sub-steps are executed. Parameters are passed from the parent-step to the sub-steps. Additionally each step can handle exceptions through a handler-step. Through Requisites checks are added before (Prerequisite) and after (Postrequisite) a step's execution. Each step uses a set of resources necessary to execute it. Resources are acquired during runtime adding dynamic behaviour to Little-JIL programs \cite{cass_little-jil/juliette:_2000} \cite{wise_using_2000}.

Communication between humans (agents) is always through parameter passing of steps. Compared to \gls{hadl} Little-JIL does not allow specification of how humans interact with each other. The \gls{dsl} has the notion of tasks \seeref{c-design} that can be executed by a step in Little-JIL.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.7\textwidth]{little-jil-step}
	\caption{Little-JIL Step \cite{wise_using_2000}}
	\label{fig-little-jil-step}
\end{figure}





\subsubsection{\gls{ws-ht} and BPEL4People}
\label{br-hip-wsht-bpel}
\gls{bpel} - more accurately \gls{ws}-\gls{bpel} - defines business processes focussing on interaction between web services. Standard \gls{bpel} does not define how humans are incorporated into business processes. Therefore an extension to \gls{bpel} has been introduced named \gls{ws}-BPEL4People \cite{_ws-bpel_2010}. BPEL4People utilizes \gls{ws-ht} specification \cite{_web_2012} as well as other \gls{ws}-* specifications.

\gls{ws-ht} \cite{_web_2012} describes humans in a service-oriented fashion through \gls{wsdl} interfaces. Hence integrating "human services" into service-oriented applications is possible. \gls{ws-ht} defines two types of interfaces: (i) an interface that provides the service described by the task and (ii) another interface that lets people handle the tasks. Each human task has a set of people assigned to it. Sequential and parallel assignment of tasks is possible. Notifications are a special form of Human Tasks that send information concerning the business process to people. Furthermore \gls{ws-ht} defines decomposition of tasks into subtasks (sequential or parallel), Lean Tasks which have reduced capabilities and input/output data type descriptions. \citeauthor{gerhards_towards_2011} \cite{gerhards_towards_2011} propose a security framework that adds authentication and authorization capabilities to \gls{ws-ht} processors.

BPEL4People \cite{_ws-bpel_2010} builds on top of \gls{ws-ht}. It adds People Activities to \gls{bpel} that are either inline Human Tasks or standalone Human Tasks (as described by \gls{ws-ht} \cite{_web_2012}).

\gls{ws-ht} and BPEL4People communication and coordination between humans are on-task basis. Whereas \gls{hadl} and specifically the proposed notion of combining it with a process-centric language \seeref{br-hadl-ps} does not have this limitation. Through this work's \gls{dsl} collaboration and coordination within a process task can be described with \gls{hadl}'s capabilities.
